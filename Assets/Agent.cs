using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

class Agent : Player
{
	protected GameState game = GameState.Instance;
    private int ID;
    private int[] playerTradeWeights = new int[4]{10,10,10,10}; //
    private List<TradeProposal> tradeHistory = new List<TradeProposal>(); //Trades proposed by agent this round
	
	public Agent(int pid, Color color) : base(pid, color)
	{
        ID = pid;
        playerType = PlayerType.AGENT;
        playerTradeWeights[ID-1] = 0;

        InitializeActions();
	}
    

    private List<Crossing> myTowns = new List<Crossing>();
    public new void DoTurn()
    {
        base.DoTurn();
        if (game.GetRound() > 1)
        {
            game.RollDice();
            if(game.moveRobber == true)
            {
                DoRobber();
            }
            EvaluateGoal();

            if (CurrentGoal != null)
                while (SelectAction() != NullAction) ;
            tradeHistory.Clear(); //Clear trade history
            //do we want to evaluate goal after every action? probably not.
            CheckBuildingsToWin();
        }
        else
        {
            DoStartingTurn();
        }
        if (game.GetRound() > 50)
        {
            Debug.Log(game.stopwatch.ElapsedMilliseconds / 1000f);
            return;
        }

        base.endTurn();
    }

    private void DoStartingTurn()
    {
        Crossing best = FindBestTarget(game.GetBoard().validTownPlacement()).Crossing;
        base.BuildTown(best);
        myTowns.Add(best);
        base.BuildRoad(FindBestRoad(best), true);
    }

    private void CheckBuildingsToWin()
    {
        int bdgs = 0;
        foreach (Card c in cards)
            if ((c.name) == "Building")
                bdgs++;
        if (Points + bdgs == 10)
        {
            cards.RemoveAll((Card bdg) => { return (bdg.name == "Building"); });
            Points += bdgs;
        }
    }

    //#region Personality
    //private Personality AgentPersonality;

    //private class Personality
    //{
    //    public Agent _agent;
    //    public delegate int ActionBias(Action a);
    //    public ActionBias ActionBias;
    //    public delegate int GoalBias(Goal a);
    //    public GoalBias PersonalityBias;
        
    //    public Personality(ActionBias b)
    //    {
    //    }

    //    public static ActionBias NeverTrade = delegate(Action a) { if (a.ActionType == Action.Type.Trade) return -1000; else return 0; };
    //}

    //private void PickPersonality()
    //{

    //}
    //#endregion


    #region Goals
    private Goal CurrentGoal { get; set; }

    public class Goal
    {
        public Goal(Agent a, Type t, int initScore = 0)
        {
            _agent = a;
            GoalType = t;
            Score = initScore;
            CalculateScore();
        }

        private Agent _agent;
        public Target target { get; private set; }
        public class Target
        {
            public Crossing Crossing { get; private set; }
            private Road[] _route = null;
            public Road Route
            {
                get
                {
                    if (RouteLength > 0)
                    {
                        for (int i = 0; i < RouteLength; i++)
                            if (_route[i].playerID == 0)
                                return _route[i];
                        return null;
                    }
                    else return null;
                }
            }
            public int RouteLength
            {
                get
                {
                    if (_route == null)
                        return 0;
                    else return _route.Length;
                }
            }

            public Target(Crossing target, Road[] route = null)
            {
                Crossing = target;
                if (route != null)
                {
                    List<Road> actualRoute = new List<Road>();
                    //ignore owned routes
                    for (int i = 0; i < route.Length; i++)
                        if (route[i].playerID == 0)
                            actualRoute.Add(route[i]);
                    _route = actualRoute.ToArray();
                }
            }
        }

        public enum Type { Town, City, Roads, Knights }
        public Type GoalType { get; private set; }

        private int _score;
        public int Score
        {
            get
            {
                if (_score < 0)
                    return 0;
                else return _score;
            }
            set
            {
                _score = value;
            }
        }

        private void CalculateScore()
        {
            Goal actual = _agent.CurrentGoal;
            _agent.CurrentGoal = null;
            switch (GoalType)
            {
                case Type.Town:
                    {
                        target = _agent.FindBestTarget(_agent.game.GetBoard().validTownPlacement(true));
                        if (target.Crossing == null)
                            Score = 0;
                        else
                        {
                            _agent.CurrentGoal = this;
                            Score = _agent.CalculateCrossingScore(target.Crossing);
                            foreach (ResourceType rt in Needs)
                                Score -= _agent.GetResourceCardValue(rt);
                        }
                        break;
                    }
                case Type.City:
                    {
                        target = _agent.FindBestTarget(_agent.myTowns);
                        if (target.Crossing == null)
                            Score = 0;
                        else
                        {
                            _agent.CurrentGoal = this;
                            Score = _agent.CalculateCrossingScore(target.Crossing) / 2;
                            foreach (ResourceType rt in Needs)
                                Score -= _agent.GetResourceCardValue(rt);
                        }
                        break;
                    }
                case Type.Knights: //Knights and Roads
                    {
                        if (_agent.game.playerWithMostKnights == _agent.ID)
                            Score = 0;
                        else
                        {
                            target = _agent.FindBestTarget(_agent.game.GetBoard().validTownPlacement(true));
                            _agent.CurrentGoal = this;
                            foreach (ResourceType rt in Needs)
                                Score -= _agent.GetResourceCardValue(rt);
                        }
                        break;
                    }
                case Type.Roads: //Knights and Roads
                    {
                        if (_agent.game.playerWithLongestRoad == _agent.ID)
                            Score = 0;
                        else
                        {
                            target = _agent.FindBestTarget(_agent.game.GetBoard().validTownPlacement(true));
                            _agent.CurrentGoal = this;
                            foreach (ResourceType rt in Needs)
                                Score -= _agent.GetResourceCardValue(rt);
                        }
                        break;
                    }
            }
            _agent.CurrentGoal = actual;
        }

        private List<ResourceType> _needs;
        public List<ResourceType> Needs
        {
            get
            {
                if (_needs == null)
                    _needs = CalculateNeeds();
                return _needs;
            }
        }
        private List<ResourceType> CalculateNeeds()
        {
            List<ResourceType> needs = new List<ResourceType>();
            switch (GoalType)
            {
                case Type.City:
                    {
                        for (int i = _agent.resources[ResourceType.ORE]; i < 3; i++)
                            needs.Add(ResourceType.ORE);
                        for (int i = _agent.resources[ResourceType.WHEAT]; i < 2; i++)
                            needs.Add(ResourceType.WHEAT);
                        break;
                    }
                case Type.Knights:
                    {
                        int knightsNeeded = _agent.game.mostKnights - _agent.knights;
                        foreach (ResourceType rt in CardCosts)
                            for (int i = _agent.resources[rt]; i < knightsNeeded; i++)
                                needs.Add(rt);
                        break;
                    }
                case Type.Roads:
                    {
                        int roadsNeeded = _agent.game.lenghtLongestRoad - _agent.game.GetBoard().LengthLongestRoad(_agent.ID);
                        foreach (ResourceType rt in RoadCosts)
                            for (int i = _agent.resources[rt]; i < roadsNeeded; i++)
                                needs.Add(rt);
                        break;
                    }
                case Type.Town:
                    {
                        int distance = target.RouteLength;
                        foreach (ResourceType rt in RoadCosts)
                            for (int i = _agent.resources[rt]; i < distance; i++)
                                needs.Add(rt);
                        foreach (ResourceType rt in TownCosts)
                            for (int i = _agent.resources[rt]; i < 1; i++)
                                needs.Add(rt);
                        break;
                    }
            }
            return needs;
        }

        public bool HasActionRelation(Action a)
        {
            if (a.ActionType == Action.Type.City && GoalType == Type.City)
                return true;
            if (a.ActionType == Action.Type.Town && GoalType == Type.Town && target.RouteLength == 0)
                return true;
            if (a.ActionType == Action.Type.Road && GoalType == Type.Town && target.RouteLength > 0)
                return true;
            if (a.ActionType == Action.Type.Road && GoalType == Type.Roads)
                return true;
            if (a.ActionType == Action.Type.Buy && GoalType == Type.Knights)
                return true;
            if (a.ActionType == Action.Type.Play && GoalType == Type.Knights)
                return true;
            return false;
        }

        public static ResourceType[] RoadCosts = new ResourceType[2] { ResourceType.WOOD, ResourceType.STONE };
        public static ResourceType[] TownCosts = new ResourceType[4] { ResourceType.WOOD, ResourceType.STONE, ResourceType.WOOL, ResourceType.WHEAT };
        public static ResourceType[] CityCosts = new ResourceType[5] { ResourceType.ORE, ResourceType.ORE, ResourceType.ORE, ResourceType.WHEAT, ResourceType.WHEAT };
        public static ResourceType[] CardCosts = new ResourceType[3] { ResourceType.WOOL, ResourceType.WHEAT, ResourceType.ORE };
    }

    private void EvaluateGoal()
    {
        Goal prev = CurrentGoal;
        CurrentGoal = null;

        LinkedList<Goal> goals = new LinkedList<Goal>();
        Goal city = new Goal(this, Goal.Type.City);
        Goal town = new Goal(this, Goal.Type.Town);
        goals.AddFirst(city);
        goals.AddFirst(town);
        goals.AddFirst(new Goal(this, Goal.Type.Knights, city.Score + town.Score / 2));
        goals.AddFirst(new Goal(this, Goal.Type.Roads, city.Score + town.Score / 2));

        int scoreTotal = 0;
        string debug = "NEW GOAL EVAL >> " + ID;
        string details = "\nDETAILS:";
        foreach (Goal g in goals)
            scoreTotal += g.Score;

        //give much greater chance for the current goal to continue if this goal very much resembles the currentgoal (the old goal)
        foreach (Goal g in goals)
        {
            details += "\nGOAL: " + g.GoalType.ToString() + ": " + g.Score;
            if (prev != null)
            {
                if (prev.GoalType == g.GoalType && prev.target != null)
                {
                    if (prev.target.Crossing == g.target.Crossing)
                    {
                        g.Score += scoreTotal;
                        scoreTotal += scoreTotal;
                    }
                }
            }
        }

        if (scoreTotal > 0)
        {
            int value = Random.Range(1, (int)scoreTotal);
            LinkedListNode<Goal> node = goals.First;
            int count = 0;

            while (value > node.Value.Score + count)
            {
                count += (int)node.Value.Score;
                node = node.Next;
            }

            Debug.Log(debug + "\nPICKED " + node.Value.GoalType.ToString() + details);
            CurrentGoal = node.Value;
        }
        else CurrentGoal = null;
    }
    #endregion

    #region Action Definitions
    public class Action
    {
        public int Weight = 10;
        private Agent _agent;

        public delegate bool Condition();
        public delegate void Function();

        //Condition asks whether the action can be performed, Function is the action to be performed
        public Condition Cond { get; private set; }
        public Function Func { get; private set; }

        public enum Type { Null, City, Town, Road, Buy, Play, Trade }
        public Type ActionType { get; private set; }

        public Action(Agent a, Type type)
        {
            _agent = a;
            ActionType = type;
            ConstructAction();
        }

        private void ConstructAction()
        {
            switch (ActionType)
            {
                case Type.Null:
                    {
                        Cond = delegate { return true; };
                        Func = delegate { }; break;
                    }
                case Type.City:
                    {
                        Cond = delegate { return (_agent.resources[ResourceType.WHEAT] > 1 && _agent.resources[ResourceType.ORE] > 2 && _agent.myTowns.Count > 0); };
                        Func = delegate { _agent.BuildCity(); }; break;
                    }
                case Type.Town:
                    {
                        Cond = delegate { return _agent.game.isAllowedToBuildTown(_agent); };
                        Func = delegate { _agent.BuildTown(); }; break;
                    }
                case Type.Road:
                    {
                        Cond = delegate { return _agent.game.isAllowedToBuildRoad(_agent); };
                        Func = delegate { _agent.BuildRoad(); }; break;
                    }
                case Type.Buy:
                    {
                        Cond = delegate { return (_agent.resources[ResourceType.ORE] > 0 && _agent.resources[ResourceType.WOOL] > 0 && _agent.resources[ResourceType.WHEAT] > 0); };
                        Func = delegate { _agent.BuyCard(); }; break;
                    }
                case Type.Play:
                    {   //don't count Building cards, we play these seperately, see DoTurn()
                        Cond = delegate { int cc = 0; foreach (Card c in _agent.cards) if (c.name != "Building") cc++; return (cc > 0); };
                        Func = delegate { _agent.PlayCard(); }; break;
                    }
                case Type.Trade:
                    {
                        Cond = delegate { return true; };
                        Func = delegate { _agent.Trade(); }; break;
                    }
            }
        }
    }

    void InitializeActions()
    {
        Actions.Add(NullAction = new Action(this, Action.Type.Null));
        Actions.Add(new Action(this, Action.Type.Town));
        Actions.Add(new Action(this, Action.Type.City));
        Actions.Add(new Action(this, Action.Type.Road));
        Actions.Add(new Action(this, Action.Type.Buy));
        Actions.Add(new Action(this, Action.Type.Play));
        Actions.Add(TradeAction = new Action(this, Action.Type.Trade));
    }

    private Action TradeAction;
    private Action NullAction;
    private List<Action> Actions = new List<Action>();

    private LinkedList<Action> GetPossibleActions()
    {
        LinkedList<Action> aa = new LinkedList<Action>();
        foreach (Action a in Actions)
            if (a.Cond.Invoke())
                aa.AddLast(a);
        return aa;
    }

    private int WeighActions(LinkedList<Action> actions)
    {
        int totalWeights = 0;
        foreach (Action a in actions)
        {
            if (CurrentGoal.HasActionRelation(a))
                a.Weight = 1000;
            else if (a.ActionType != Action.Type.Trade)
                a.Weight = 10;
            //a.Weight += AgentPersonality.PersonalityBias.Invoke(a);
            totalWeights += Mathf.Max(0, a.Weight);
        }
        return totalWeights;
    }

    private Action SelectAction()
    {
        LinkedList<Action> actions = GetPossibleActions();
        int total = WeighActions(actions);
        int value = new System.Random().Next(1, total + 1);

        LinkedListNode<Action> node = actions.First;
        int count = 0;

        //debugging only
        string debug = "NEW ACTION EVAL FOR GOAL " + CurrentGoal.GoalType.ToString().ToUpper() + " >> " + ID + ": " + actions.Count + " actions, [" + value + "]";
        string details = "\nDETAILS";
        LinkedListNode<Action> test = actions.First;
        while (test != null)
        {
            details += "\n" + test.Value.ActionType.ToString() + ":" + test.Value.Weight;
            test = test.Next;
        }
        //--

        while (value > node.Value.Weight + count)
        {
            count += node.Value.Weight;
            node = node.Next;
        }

        Action selection = node.Value;
        selection.Func.Invoke();
        Debug.Log(debug + "\nPICKED " + selection.ActionType.ToString() + details);
        return selection;
    }
    #endregion

    #region Action Functions
    protected void BuildTown()
    {
        if (CurrentGoal.GoalType == Goal.Type.Town)
            if (base.BuildTown(CurrentGoal.target.Crossing))
                myTowns.Add(CurrentGoal.target.Crossing);
        //falsely picked this action otherwise!
    }

    protected void BuildCity()
    {
        if (CurrentGoal.GoalType == Goal.Type.City)
        {
            base.BuildCity(CurrentGoal.target.Crossing);
            myTowns.Remove(CurrentGoal.target.Crossing);
        }
        //falsely picked this action otherwise!
    }

    protected void BuildRoad()
    {
        if (CurrentGoal.target.Route != null && CurrentGoal.target.RouteLength > 0)
        {
            Road targetRoad = CurrentGoal.target.Route;
            if (targetRoad != null)
                base.BuildRoad(targetRoad);
        }
    }

    protected void BuyCard()
    {
        base.GetCard();
    }

    protected void PlayCard()
    {
        Card picked = null;
        foreach (Card c in cards)
        {
            picked = c;
            switch (c.category)
            {
                case "Development":
                    switch (c.name)
                    {
                        case "Invention":
                            PerformInventionCard();
                            goto Discard;
                        case "Roadconstruction":// done by gamestate
                            PerformRoadContructionCard();
                            goto Discard;
                        case "Monopoly":
                            game.PlayMonopoly(FindBestResourceType(), playerID);
                            goto Discard;
                    }
                    break;
                case "Knight":
                    knights++;
                    game.CheckKnights(playerID);
                    DoRobber();
                    goto Discard;
                case "Building":
                    //Never play building in this function, see DoTurn() for when this happens
                    break;
            }
        }
        Discard:
        {
            if (picked != null)
                cards.Remove(picked);
        }
    }

    private ResourceType FindBestResourceType()
    {
        ResourceType[] rts = new ResourceType[5] { ResourceType.ORE, ResourceType.STONE, ResourceType.WOOL, ResourceType.WHEAT, ResourceType.WOOD };
        ResourceType bestRT = ResourceType.WOOD;
        int best = int.MaxValue;
        foreach (ResourceType rt in rts)
        {
            int val = GetResourceCardValue(rt);
            if (val < best)
            {
                best = val;
                bestRT = rt;
            }
        }
        return bestRT;
    }

    private void PerformInventionCard()
    {
        for (int i = 0; i < 2; i++)
            resources[FindBestResourceType()]++;
    }

    private void PerformRoadContructionCard()
    {
        for (int i = 0; i < 2; i++)
        {
            if (CurrentGoal.target.RouteLength > 0)
                base.BuildRoad(CurrentGoal.target.Route, true);
            else base.BuildRoad(FindBestRoad(CurrentGoal.target.Crossing), true);
        }
    }

    protected void Trade()
    {
        //picked using TradeAction.Weight, default is 10
        //deliberate on with who we trade (bank?), and give/receive what/how many. base it on goal, use GetResourceTileValue/GetResourceCardValue to value resources

        //Pick player to trade with
        int totalPlayerTradeWeights = 0;
        foreach (int w in playerTradeWeights)
            totalPlayerTradeWeights += w;

        int random = new System.Random().Next(totalPlayerTradeWeights);

        int player = 1;
        int count = 0;
        while (random > playerTradeWeights[player - 1] + count)
        {
            count += playerTradeWeights[player - 1];
            player++;
        }

        int counter = 0;

        TradeProposal tradeProposal = null;
        ResourceType sending = ResourceType.NONE;
        ResourceType receiving = ResourceType.NONE;
        while ((tradeHistory.Contains(tradeProposal) || tradeProposal == null) && counter < 50)
        {
            counter++;
            tradeProposal = new TradeProposal(this, game.GetPlayer(player));

            //Build dictionary with resource values
            Dictionary<ResourceType, int> values = new Dictionary<ResourceType, int>();
            Dictionary<ResourceType, int> inValues = new Dictionary<ResourceType, int>();
            foreach (ResourceType rt in System.Enum.GetValues(typeof(ResourceType)))
            {
                if (rt == ResourceType.ANY || rt == ResourceType.NONE)
                    continue;
                float v = GetResourceCardValue(rt);
                values.Add(rt, (int)v);
                inValues.Add(rt, (int)(((1 / v)) * 1000));
            }


            //Choose random weighted random receiving and sending value
            receiving = Util.getWeightedRandomValue(values);
            sending = Util.getWeightedRandomValue(inValues);

            //Bit silly to be trading equal resources or trying to trade resources we don't have
            if (receiving == sending || resources[sending] == 0)
            {
                tradeProposal = null;
                continue;
            }

            //Decide how many we want to receive based on need
            if (CurrentGoal.Needs.Contains(receiving))
                tradeProposal.receiving[receiving] = (uint)CurrentGoal.Needs.Where(x => x.Equals(receiving)).Count();
            else
                tradeProposal.receiving[receiving] = 1;

            //Decide how many we want to give based on value (for other) and availability
            int receivingValue = (int)(values[receiving] * tradeProposal.receiving[receiving]);
            tradeProposal.sending[sending] = (uint)Mathf.Min(resources[sending],Mathf.Max(1, (uint)Mathf.Floor(receivingValue / GetResourceCardValue(sending))));

        }
        //Send proposal
        if (counter >= 50) return;


        //Choose bank or player
        if (GetHarborRate(sending)*tradeProposal.receiving[receiving] <= tradeProposal.sending[sending])
        {
            //trade with da bank
            Debug.Log("Player " + ID + " is trading with the bank");
            resources[sending] -= (int)(GetHarborRate(sending)*tradeProposal.receiving[receiving]);
            resources[receiving] += (int)tradeProposal.receiving[receiving];
            return;

        }
        

        Debug.Log("Player " + ID + " is trying to trade with player " + player);
        if (tradeProposal == null || game.GetPlayer(player) == null)
            return;
        if (game.GetPlayer(player).AcceptTrade(tradeProposal))
        {
            tradeProposal.PerformTrade();

            playerTradeWeights[player - 1]++; //Increase willigness to trade with this player
        }
        else
        {
            playerTradeWeights[player - 1]--;
        }

        tradeHistory.Add(tradeProposal);
    }

    public override bool AcceptTrade(TradeProposal t)
    {
        bool counterOffer = false;
        TradeProposal counter = t.ReverseCopy();
        counter.counter = true;
        int r = new System.Random().Next(10);
        if (playerTradeWeights[t.player.playerID - 1] <= r)
        {
            Debug.Log("Player " + ID + " does not like player " + t.player.playerID);
            return false; //Not willing to trade with this person
        }

        int sendingValue = 0; //Value of what I am sending
        int receivingValue = 0; //Value of what I am receiving
        //How much of the resources he is requesting am I willing to give away?
        foreach (ResourceType rt in t.receiving.Keys)
        {
            if (t.receiving[rt] == 0)
                continue;

            if (resources[rt] > 0)
            {
                //I have some of this resource\
                int need = 0;
                if(CurrentGoal != null)
                    need = CurrentGoal.Needs.FindAll(res => res == rt).Count(); //How many do I need
                if ((resources[rt] - need) > 0 && (resources[rt] - need) < t.receiving[rt])
                {
                    //I'm willing to give some away, but not all he wants
                    if (t.counter)
                        return false; //We're not countering counters
                    counter.sending[rt] = (uint)(resources[rt] - need);
                    counterOffer = true;
                }
            }
            sendingValue += (int)(GetResourceCardValue(rt) * t.receiving[rt]);
            receivingValue += (int)(GetResourceCardValue(rt) * t.receiving[rt]);
        }

        if (!counterOffer)
        {
            //Do I think what he is giving me worth it?
            if (sendingValue <= receivingValue)
            {
                Debug.Log("Player " + ID + " accepts trade from player " + t.player.playerID);
                return true;
            }
            else
            {
                if (t.counter)
                    return false; //We're not countering counters
                //choose a resource to give instead
                Dictionary<ResourceType, int> inValues = new Dictionary<ResourceType, int>();
                foreach (ResourceType rt in System.Enum.GetValues(typeof(ResourceType)))
                {
                    if (rt == ResourceType.ANY || rt == ResourceType.NONE)
                        continue;
                    float v = GetResourceCardValue(rt);
                    inValues.Add(rt, (int)(((1 / v)) * 1000));
                }
                ResourceType sending = Util.getWeightedRandomValue(inValues);
                foreach (ResourceType rt in t.receiving.Keys)
                   counter.sending[rt] = 0;
                counter.sending[sending] = (uint)Mathf.Min(resources[sending], Mathf.Max(1, (uint)Mathf.Floor(receivingValue / GetResourceCardValue(sending))));
                counterOffer = true;

            }
        }

        if (counterOffer && !tradeHistory.Contains(counter))
        {
            Debug.Log("Player " + ID + " is making a counter offer to " + counter.player.playerID);
            tradeHistory.Add(counter);
            if (t.player.AcceptTrade(counter))
            {
                counter.PerformTrade();

                playerTradeWeights[counter.player.playerID - 1]++; //Increase willigness to trade with this player
            }
            else
            {
                playerTradeWeights[counter.player.playerID - 1]--;
                Debug.Log("Player " + ID + " wanted to do a counteroffer but " + counter.player.playerID + "refused.");
                return false;
            }
        }

        Debug.Log("Player " + ID + " wanted to do a counteroffer but " + counter.player.playerID + "had already once refused the same trade.");
        return false;
    }

    protected void DoRobber()
    {
        ResourceTile bestResourceTile = game.GetBoard().getBestRobberTile(playerID);
        game.MoveRobber(bestResourceTile);
        List<Crossing> crossings = new List<Crossing>();
        foreach(Crossing c in bestResourceTile.Crossings)
        {
            if(c.playerID != 0)
            {
                if(c.playerID != playerID)
                {
                    crossings.Add(c);
                }
            }
        }

        Crossing crossingToRob = new Crossing();
        int currentHighestScore = 0;
        foreach(Crossing c in crossings)
        {
            if (game.getPlayerPoints(c.playerID) > currentHighestScore)
            {
                currentHighestScore = game.getPlayerPoints(c.playerID);
                crossingToRob = c;
            }
        }

        game.RobPlayer(crossingToRob.playerID, crossingToRob);
    }

    /*DEPRECATED
    //protected void Bank(ResourceType give, ResourceType receive)
    //{
    //    int require = GetHarborRate(give);
    //    if (resources[give] >= require)
    //    {
    //        resources[give] -= require;
    //        resources[receive] += 1;
    //    }
    //}

    //protected void Trade(Player target, ResourceType give, ResourceType receive)
    //{
    //    //add heuristic for trade amounts
    //    uint giveAmount = 0;
    //    uint receiveAmount = 0;

    //    TradeProposal tp = new TradeProposal(this, target);
    //    tp.sending[give] += giveAmount;
    //    tp.recieving[receive] += receiveAmount;
    //}*/
    #endregion

    #region Heuristics



    /// <summary>
    /// Returns a score for a given ResourceTile, score can be compared with GetResourceCardValue.
    /// </summary>
    private int GetResourceTileValue(ResourceTile rt)
    {
        //TODO: should also give a score to harbors....
        //tile value is based on needs, harbors, chance, and rolls left
        int score = ResourceNumberToValue(rt.number);
        if (GetHarborRate(rt.resource.resourceType) == 2)
            score *= 2;
        if (CurrentGoal != null && CurrentGoal.Needs.Contains(rt.resource.resourceType))
            score *= 2;
        score *= PredictRollsLeft();
        return score;
    }

    /// <summary>
    /// Returns a score for a given ResourceType, score can be compared with GetResourceTileValue.
    /// </summary>
    private int GetResourceCardValue(ResourceType rt)
    {
        //card value is based on needs, harbors
        int score = 36;
        if (GetHarborRate(rt) == 2)
            score *= 2;
        if (CurrentGoal != null && CurrentGoal.Needs.Contains(rt))
            score *= 2;
        return score;
    }

    /// <summary>
    /// Returns a score for a given Crossing.
    /// </summary>
    private int CalculateCrossingScore(Crossing c)
    {
        int score = 0;
        foreach (ResourceTile rt in c.adjacentTiles())
            score += GetResourceTileValue(rt);
        return score;
    }

    /// <summary>
    /// Calculate the distance between the nearest town and c
    /// TODO: Shortest path to c needs to be valid
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    private Road[] TownToCrossingDistance(Crossing c)
    {
        if (myTowns.Count >= 2)
        {
            Road[] bestRoute = null;
            int bestDistance = int.MaxValue;
            foreach (Crossing myC in myTowns)
            {
                Road[] route = myC.CrossingRoute(c, ID);
                int distance = route.Length;

                if (distance < bestDistance)
                {
                    bestDistance = distance;
                    bestRoute = route;
                }
            }
            return bestRoute;
        }
        else return null;
    }

    /// <summary>
    /// Returns a crossing with the highest score. (takes into account road costs)
    /// </summary>
    private Goal.Target FindBestTarget(List<Crossing> cc)
    {
        Crossing bestCrossing = null;
        Road[] bestRoute = null;
        int fitness = int.MinValue;

        foreach (Crossing c in cc)
        {
            int thisFitness = CalculateCrossingScore(c);
            Road[] route = null;
            if (!myTowns.Contains(c))
            {
                route = TownToCrossingDistance(c);
                if (route != null)
                {
                    foreach (ResourceType rt in Goal.RoadCosts)
                        for (int i = resources[rt]; i < route.Length; i++)
                            thisFitness -= GetResourceCardValue(rt);
                }
            }

            if (thisFitness > fitness)
            {
                bestCrossing = c;
                if (route != null)
                    bestRoute = route;
                fitness = thisFitness;
            }
        }
        return new Goal.Target(bestCrossing, bestRoute);
    }

    //DEPRECATED
    //private Road FindBestRoad(List<Road> rr)
    //{
    //    Dictionary<Crossing, Road> cr = new Dictionary<Crossing, Road>();

    //    foreach (Road r in rr)
    //    {
    //        foreach (Crossing nc in r.adjecentCrossings())
    //            if (!cr.ContainsKey(nc) && (nc.playerID == 0 || nc.playerID == this.playerID))
    //                cr.Add(nc, r);
    //    }

    //    //can be shorter, but I couldn't be bothered :(
    //    List<Crossing> list = new List<Crossing>();
    //    foreach (Crossing cl in cr.Keys)
    //        list.Add(cl);

    //    return cr[FindBestCrossing(list)];
    //}

    /// <summary>
    /// for each road near crossing, check the adjacent crossings of the outer crossing. 
    /// check each of those crossings (2 steps away from crossing c) for the best crossing
    /// </summary>
    private Road FindBestRoad(Crossing c)
    {
        List<Road> rr = c.adjecentRoads();
        Dictionary<Crossing, Road> cr = new Dictionary<Crossing, Road>();

        foreach (Road r in rr)
        {
            Crossing n = r.adjecentCrossings().Find(x => x != c);

            foreach (Crossing nc in n.crossingNeighbours())
                if (nc != c && (nc.playerID == 0 || nc.playerID == this.playerID))
                    cr.Add(nc, r);
        }

        //can be shorter, but I couldn't be bothered :(
        List<Crossing> list = new List<Crossing>();
        foreach (Crossing cl in cr.Keys)
            list.Add(cl);

        return cr[FindBestTarget(list).Crossing];
    }
    #endregion

    public void throwAwayResources()
    {
        ResourceType[] goalCost = Goal.RoadCosts;
        Dictionary<ResourceType, int> goalCostDic = new Dictionary<ResourceType, int>();

        if (CurrentGoal != null)
        {
            switch (CurrentGoal.GoalType)
            {
                case Goal.Type.City:
                    goalCost = Goal.CityCosts;
                    break;
                case Goal.Type.Knights:
                    goalCost = Goal.CardCosts;
                    break;
                case Goal.Type.Roads:
                    goalCost = Goal.RoadCosts;
                    break;
                case Goal.Type.Town:
                    goalCost = Goal.TownCosts;
                    break;
            }
        }

        foreach (ResourceType resourceType in goalCost)
        {
            if(goalCostDic.ContainsKey(resourceType))
            {
                goalCostDic[resourceType]++;
            }
            else
            {
                goalCostDic[resourceType] = 1;
            }
        }

        ResourceType[] r = new ResourceType[5] {ResourceType.WHEAT, ResourceType.WOOD, ResourceType.WOOL, ResourceType.STONE, ResourceType.ORE};


        while (resources.Sum(x => x.Value) > 7)
        {
            Debug.Log("test");
            int rnd = Random.Range(0, 5);
            if(resources[r[rnd]] <= 0)
            {
                continue;
            }
            else if(goalCostDic.ContainsKey(r[rnd]))
            {
                if(goalCostDic[r[rnd]] < resources[r[rnd]])
                {
                    throwAwayResource(r[rnd]);
                    continue;
                }
                else
                {
                    continue;
                }
            }
            else
            {
                throwAwayResource(r[rnd]);
                continue;
            }
            
        }







        return;
    }
}