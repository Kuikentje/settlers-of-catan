using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour
{

    public Resource wheat = new Wheat();
    public Resource desert = new Desert();
    public Resource stone = new Stone();
    public Resource ore = new Ore();
    public Resource wool = new Wool();
    public Resource wood = new Wood();


    //params
    public GameObject roadPrefab;
    public GameObject crossingPrefab;

    //Lists
    public List<Crossing> crossings;
    public List<Road> roads;
    public List<Tile> tiles;
    public List<ResourceTile> resourceTileList = new List<ResourceTile>();

    private GameState game = GameState.Instance;

    public bool boardInitialized = false;

    // Use this for initialization
    void Awake()
    {
        //Create lists
        crossings = new List<Crossing>();
        roads = new List<Road>();
        tiles = new List<Tile>();

        //Create edges
        Dictionary<Edge, Face> edges = new Dictionary<Edge, Face>(new EdgeComparer());


        for (int i = 0; i < transform.childCount; i++)
        {
            Tile tile = transform.GetChild(i).gameObject.GetComponent<Tile>();
            tiles.Add(tile);

            List<Edge> list = tile.getEdges();
            foreach (Edge edge in list)
                edges.Add(edge, tile.Face);
        }

        //Create random resources
        ResourceTile[] resourceTiles = transform.GetComponentsInChildren<ResourceTile>();
        List<Resource> resources = new List<Resource> { new Wood(), new Wood(), new Wood(), new Wood(), new Wool(), new Wool(), new Wool(), new Wool(), new Wheat(), new Wheat(), new Wheat(), new Wheat(), new Ore(), new Ore(), new Ore(), new Stone(), new Stone(), new Stone() };
        for (int i = 0; i < 18; i++)
        {
            int r = (int)Random.Range(0, resources.Count);
            resourceTiles[i].resource = resources[r];
            resources.RemoveAt(r);
        }

        //Create harbours
        HarbourTile[] harbourTiles = transform.GetComponentsInChildren<HarbourTile>();
            List<HarbourType> harbours = new List<HarbourType> { HarbourType.ORE, HarbourType.STONE, HarbourType.WHEAT, HarbourType.WOOD, HarbourType.WOOL, HarbourType.THREEFORONE, HarbourType.THREEFORONE, HarbourType.THREEFORONE, HarbourType.THREEFORONE};
            for (int i = 0; i < 9; i++)
            {
                int r = (int)Random.Range(0, harbours.Count);
                harbourTiles[i].type = harbours[r];
                harbours.RemoveAt(r);
            }



        DCEL dcel = new DCEL(edges);
        roadPrefab = Resources.Load("Road", typeof(GameObject)) as GameObject;
        crossingPrefab = Resources.Load("Crossing", typeof(GameObject)) as GameObject;

        //Create roads
        foreach (Edge edge in dcel.processedEdges.Keys)
        {
            GameObject road = Instantiate(roadPrefab);
            Road r = road.AddComponent<Road>();
            roads.Add(r);
            r.halfEdge = dcel.processedEdges[edge];
            r.halfEdge.road = r;
            r.halfEdge.Twin.road = r;
            //Position
            Vector3 p = new Vector3((r.halfEdge.Origin.position.x + r.halfEdge.Twin.Origin.position.x) / 2, (r.halfEdge.Origin.position.y + r.halfEdge.Twin.Origin.position.y) / 2, 0);
            road.transform.position = p;

            //Rotation
            if (Mathf.Round(r.halfEdge.Origin.position.x) > Mathf.Round(r.halfEdge.Twin.Origin.position.x) && Mathf.Round(r.halfEdge.Origin.position.y) > Mathf.Round(r.halfEdge.Twin.Origin.position.y))
                road.transform.rotation = Quaternion.Euler(0, 0, 60);

            if (Mathf.Round(r.halfEdge.Origin.position.x) < Mathf.Round(r.halfEdge.Twin.Origin.position.x) && Mathf.Round(r.halfEdge.Origin.position.y) < Mathf.Round(r.halfEdge.Twin.Origin.position.y))
                road.transform.rotation = Quaternion.Euler(0, 0, 60);

            if (Mathf.Round(r.halfEdge.Origin.position.x) < Mathf.Round(r.halfEdge.Twin.Origin.position.x) && Mathf.Round(r.halfEdge.Origin.position.y) > Mathf.Round(r.halfEdge.Twin.Origin.position.y))
                road.transform.rotation = Quaternion.Euler(0, 0, -60);

            if (Mathf.Round(r.halfEdge.Origin.position.x) > Mathf.Round(r.halfEdge.Twin.Origin.position.x) && Mathf.Round(r.halfEdge.Origin.position.y) < Mathf.Round(r.halfEdge.Twin.Origin.position.y))
                road.transform.rotation = Quaternion.Euler(0, 0, -60);
        }


        //Create crossings
        foreach (Vertex vertex in dcel.vertexDict.Values)
        {
            GameObject crossing = Instantiate(crossingPrefab);
            Crossing c = crossing.GetComponent<Crossing>();
            crossings.Add(c);
            c.vertex = vertex;
            c.vertex.crossing = c;
           

            //Position
            Vector3 p = new Vector3(c.vertex.position.x, c.vertex.position.y, 0);
            crossing.transform.position = p;
        }

        //Create random numbers
        List<int> numbers = new List<int> { 6, 6, 8, 8, 2, 3, 3, 4, 4, 5, 5, 9, 9, 10, 10, 11, 11, 12 };
        for (int i = 0; i < 18; i++)
        {
            int r = 0;
            bool valid = false;

            while (!valid)
            {
                valid = true;
                r = (int)Random.Range(0, numbers.Count);
                if (resourceTiles[r].number != 7)
                    valid = false;
                else if (i >= 4)
                    valid = true;
                else
                {

                    valid = true;
                    foreach (Tile tile in resourceTiles[r].Tiles)
                    {
                        if (tile is ResourceTile)
                            if (((ResourceTile)tile).number == 6 || ((ResourceTile)tile).number == 8)
                                valid = false;
                    }
                }

            }
            resourceTiles[r].number = numbers[i];

        }
        foreach (ResourceTile res in resourceTiles)
        {
            resourceTileList.Add(res);
        }
        validTownPlacement();

        boardInitialized = true;
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// Generates a list of crossings where a town can be validly played by the current player.
    /// </summary>
    /// <returns>List of valid crossings</returns>
    public List<Crossing> validTownPlacement(bool anywhere = false)
    {
        List<Crossing> list = new List<Crossing>();
        foreach (Crossing crossing in crossings)
        {
            if ((GameState.Instance.GetRound() == 1 && game.GetPlayer(game.GetCurrentPlayerID()).townCount < 2) || anywhere)
            {
                if (crossing.isValidPlacement())
                    list.Add(crossing);
            }
            else
            {
                if (crossing.checkRoads() && crossing.isValidPlacement())
                    list.Add(crossing);
            }
        }
        return list;
    }

    public List<Road> validRoadPlacement()
    {
        List<Road> list = new List<Road>();
        foreach (Road road in roads)
        {
            if (road.isValidPlacement())
                list.Add(road);
        }
        return list;
    }

    public List<ResourceTile> getTilesByNumber(int roll)
    {
        List<ResourceTile> resList = new List<ResourceTile>();
        foreach(ResourceTile tile in resourceTileList)
        {
            if(tile.number == roll)
            {
                resList.Add(tile);
            }
        }
        return resList;
    }

    public int  LengthLongestRoad(int pid)
    {
        // find all the crossings of the player
        List<Crossing> startCrossings = new List<Crossing>();
        bool hasTown = false;
        foreach (Crossing crossing in crossings)
            if (crossing.playerID == pid)
            {
                startCrossings.Add(crossing);
                hasTown = true;
            }

        // if none then return 0
        if (!hasTown)
            return 0;

        // initialise
        List<Road> visitedRoads = new List<Road>();
        int longest = 0;

        // look at longest road starting in each crossing
        foreach (Crossing c in startCrossings)
        {
            int t = getLengthRoad(c, visitedRoads, pid);
            if (t > longest)
                longest = t;
        }

        return longest;
    }

    // returns lenght of longest road of player with id pid, starting in c, and not using the visited roads
    private int getLengthRoad(Crossing c, List<Road> visitedRoads, int pid)
    {
        List<Road> newRoads = getUnvisitedRoads(c, visitedRoads);
        if (newRoads.Count == 0)
            return 0;

        int longest = 0;
        foreach (Road r in newRoads)
        {
            if (r.playerID != pid)
                continue;

            //add road to the visited roads
            List<Road> tempVisitedRoads = new List<Road>();
            tempVisitedRoads.AddRange(visitedRoads);
            tempVisitedRoads.Add(r);
            
            //find new crossing
            Crossing newCrossing;
            List<Crossing> aCrossings = r.adjecentCrossings();
            if (aCrossings[0] == c)
                newCrossing = aCrossings[1];
            else
                newCrossing = aCrossings[0];

            int t = 1 + getLengthRoad(newCrossing, tempVisitedRoads, pid);
            if (t > longest)
                longest = t;
        }
        return longest;
    }

    public List<Road> getUnvisitedRoads(Crossing c, List<Road> visitedRoads)
    {
        List<Road> uRoads = new List<Road>();
        List<Road> aRoads =  c.adjecentRoads();
        foreach (Road r in aRoads)
            if (!visitedRoads.Contains(r))
                uRoads.Add(r);
        return uRoads;
    }

    public ResourceTile getBestRobberTile(int pid)
    {
        ResourceTile resourceTile = new ResourceTile();
        int currentHighestPointCounter = -1;
        foreach(ResourceTile res in resourceTileList)
        {
            int pointCounter = 0;
            foreach(Crossing crossing in res.Crossings)
            {
                if(crossing.playerID != 0)
                {
                    if(crossing.playerID != pid)
                    {
                        if(crossing.isCity)
                        {
                            pointCounter += 2;
                        }
                        else
                        {
                            pointCounter++;
                        }
                    }
                }
                
            }
            if(pointCounter > currentHighestPointCounter)
            {
                currentHighestPointCounter = pointCounter;
                resourceTile = res;
            }
        }
        return resourceTile;
    }

    public int GetPoints(int pid)
    {
        int points = 0;

        foreach (Crossing c in crossings)
            if (c.playerID == pid)
                if (c.isCity)
                    points += 2;
                else
                    points++;
        return points;
    }

}


