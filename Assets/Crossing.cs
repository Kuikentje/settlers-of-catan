using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Crossing : MonoBehaviour
{

    public Vertex vertex;
    public int playerID;
    GameState game = GameState.Instance;
    public bool isCity = false;
    public Sprite city;
    public Sprite town;
    // Use this for initialization
    void Start()
    {
        Vector3 p = transform.position;
        p.z--;
        transform.position = p;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseOver()
    {
        if (game.townHover && playerID == 0)
        {
            List<Crossing> towns = GameState.Instance.buildableTowns;
            if (towns.FindIndex(r => r == this) >= 0)
            {
                this.GetComponent<Renderer>().material.color = game.humanPlayer.playerColor;
            }
        }
    }

    void OnMouseExit()
    {
        if(playerID == 0)
        this.GetComponent<Renderer>().material.color = Color.white;
    }

    void OnMouseDown()
    {
        if(GameState.Instance.townHover)
        {
            GameState.Instance.GetPlayer(GameState.Instance.GetCurrentPlayerID()).BuildTown(this);
        }
        else if (game.buildCity&&playerID>0&&game.GetCurrentPlayerID()==playerID)
        {
            GameState.Instance.GetPlayer(GameState.Instance.GetCurrentPlayerID()).BuildCity(this);
        }
        else if (game.robTown && playerID != 0)
        {
            game.RobPlayer(playerID, this);
        }
    }
    

    public List<Crossing> crossingNeighbours()
    {
        List<Crossing> list = new List<Crossing>();
        foreach (Vertex v in vertex.VertexNeighbours)
            list.Add(v.crossing);

        return list;
    }

    public List<Road> adjecentRoads()
    {
        List<Road> list = new List<Road>();
        foreach (HalfEdge he in vertex.adjecentHalfEdges())
            list.Add(he.road);
        return list;
    }

    public List<HarbourTile> adjecentHarbourTiles()
    {
        List<HarbourTile> list = new List<HarbourTile>();
        foreach (Face f in vertex.adjecentFaces())
            if (f.tile as HarbourTile != null)
                list.Add(f.tile as HarbourTile);
        return list;
    }


	public bool placeTown()
	{
		//Check valid placement
		if (isValidPlacement () && checkRoads()) {
			playerID = GameState.Instance.GetCurrentPlayerID();
            Color c = game.GetPlayer(game.GetCurrentPlayerID()).playerColor;
            c.a *= 20;
            this.GetComponent<SpriteRenderer>().sprite = town;
            this.GetComponent<Renderer>().material.color = c;
            transform.localScale = new Vector3(3f, 3f, 3f);
			return true;
		}
		return false;
	}

    public bool placeCity()
    {

        this.GetComponent<SpriteRenderer>().sprite = city;
        transform.localScale = new Vector3(3f, 3f, 3f);
        Vector3 p = transform.position;
        p.y += 0.5f;
        transform.position = p;
        this.GetComponent<SpriteRenderer>().enabled = true;

        isCity = true;
        return true;
    }

    /// <summary>
    /// Check if there are roads belonging to the current player adjecent to this crossing.
    /// </summary>
    /// <returns></returns>
    public bool checkRoads()
    {
        if(GameState.Instance.GetRound() <= 1 && game.GetPlayer(game.GetCurrentPlayerID()).townCount <2)
        {
            return true;
        }
        foreach (Road r in adjecentRoads())
        {
            if (r.playerID == GameState.Instance.GetCurrentPlayerID())
                return true;
        }
        return false;
    }

	/// <summary>
	/// Checks if a town can be placed here, considering wether or not a town is already here,
	/// and if there are towns nearby.
	/// Does not take into account the colour of the town, and therefore also does not check if the town is connected to a road.
	/// Road connection must be checked seperately so this function can be used in the starting phase.
	/// </summary>
	/// <returns><c>true</c>, if placement is valid, <c>false</c> otherwise.</returns>
	public bool isValidPlacement()
	{
        //Check if a town is already placed
		if (playerID != 0)
			return false;

        //Check  if there is at least one adjecent resource tile
        bool resourcetile = false;
        foreach (Tile t in adjecentTiles())
            if (t is ResourceTile)
            {
                resourcetile = true;
            }
        if (!resourcetile) return false;

        //Check if neighbouring crossings are free
		foreach (Vertex v in vertex.VertexNeighbours)
			if (v.crossing.playerID != 0)
				return false;
		return true;
	}

    public List<ResourceTile> adjacentTiles()
    {
        List<ResourceTile> rt = new List<ResourceTile>();
        foreach (Tile t in adjecentTiles())
            if (t is ResourceTile)
                rt.Add((ResourceTile)t);
        return rt;
    }

    private IEnumerable<Tile> adjecentTiles()
    {
        List<Tile> list = new List<Tile>();
        foreach (Face f in vertex.adjecentFaces())
            list.Add(f.tile);
        return list;
    }

    public int DistanceTo(Crossing goal)
    {
        if (this == goal)
            return 0;
        Queue<KeyValuePair<HalfEdge,int>> qeueu = new Queue<KeyValuePair<HalfEdge,int>>();
        List<Vertex> done = new List<Vertex>();
        foreach (HalfEdge e in vertex.adjecentHalfEdges())
            qeueu.Enqueue(new KeyValuePair<HalfEdge,int>(e,1));
        done.Add(vertex);
        while (qeueu.Count > 0)
        {            
            KeyValuePair<HalfEdge,int> kvp = qeueu.Dequeue();
            HalfEdge e = kvp.Key;
            Crossing c = e.Next.Origin.crossing;
            if (done.Contains(c.vertex))
                continue;
            done.Add(c.vertex);
            if (c == goal)
                return kvp.Value;
            foreach (HalfEdge ee in c.vertex.adjecentHalfEdges())
                qeueu.Enqueue(new KeyValuePair<HalfEdge, int>(ee, kvp.Value + 1));
        }
        return -1;
    }

    public Road[] CrossingRoute(Crossing goal, int p=0)
    {
        p = p == 0 ? game.GetCurrentPlayerID() : p;
        List<Road> results = new List<Road>();

        Queue<KeyValuePair<HalfEdge, List<Road>>> qeueu = new Queue<KeyValuePair<HalfEdge, List<Road>>>();
        List<Vertex> done = new List<Vertex>();
        foreach (HalfEdge e in vertex.adjecentHalfEdges())
            qeueu.Enqueue(new KeyValuePair<HalfEdge, List<Road>>(e, new List<Road> { e.road }));
        done.Add(vertex);
        while (qeueu.Count > 0)
        {
            KeyValuePair<HalfEdge, List<Road>> kvp = qeueu.Dequeue();
            HalfEdge e = kvp.Key;

            if (e.road.playerID != 0)
                continue;

            Crossing c = e.Next.Origin.crossing;
            if (done.Contains(c.vertex))
                continue;
            done.Add(c.vertex);

            if (!(c.playerID == p || c.playerID == 0))
                continue;

            if (c == goal)
                return kvp.Value.ToArray();

            foreach (HalfEdge ee in c.vertex.adjecentHalfEdges())
            {
                List<Road> temp = new List<Road> ( kvp.Value );
                temp.Add(ee.road);
                qeueu.Enqueue(new KeyValuePair<HalfEdge, List<Road>>(ee,temp ));
            }
        }
        return results.ToArray();
    }
    
    }
