﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class that represents a DCEL (Doubly connected edge list) datastructure. 
/// Based on desrciption from the book Computational Geometry by Mark de Berg et al. 
/// Construction based on http://cs.stackexchange.com/questions/2450/constructing-of-double-connected-edge-list-dcel.
/// </summary>
public class DCEL
{

    public Dictionary<Edge, HalfEdge> processedEdges;
    public Dictionary<Position, Vertex> vertexDict;

    public DCEL(Dictionary<Edge, Face> edges)
    {
        //Unbounded face
        Face unbounded = new Face();
        vertexDict = new Dictionary<Position, Vertex>(new PositionComparer());
        Dictionary<Position, List<HalfEdge>> edgeDict2 = new Dictionary<Position, List<HalfEdge>>(new PositionComparer());
        //Create the vertices
        foreach (Edge edge in edges.Keys)
        {
            if (!vertexDict.ContainsKey(edge.p1))
            {
                vertexDict.Add(edge.p1, new Vertex(edge.p1));
                edgeDict2.Add(edge.p1, new List<HalfEdge>());
            }
        }


        //Create half-edges
        processedEdges = new Dictionary<Edge, HalfEdge>(new EdgeComparer());

        foreach (Edge edge in edges.Keys)
        {
            Edge e2 = new Edge(edge.p2, edge.p1);
            if (!processedEdges.ContainsKey(e2))
            {
                HalfEdge he1 = new HalfEdge(vertexDict[edge.p1]);
                vertexDict[edge.p1].IncidentEdge = he1;
                edgeDict2[edge.p1].Add(he1);

                he1.IncidentFace = edges[edge];
                edges[edge].OuterComponent = he1;

                HalfEdge he2 = new HalfEdge(vertexDict[edge.p2]);
                vertexDict[edge.p2].IncidentEdge = he2;
                edgeDict2[edge.p2].Add(he2);

                if (edges.ContainsKey(e2))
                {
                    he2.IncidentFace = edges[e2];
                    edges[e2].OuterComponent = he2;
                }
                else
                {
                    he2.IncidentFace = unbounded;
                    unbounded.OuterComponent = he2;
                }
                he1.Twin = he2;
                he2.Twin = he1;
                processedEdges.Add(edge, he2);

            }
            else
            {
                processedEdges[new Edge(edge.p2, edge.p1)].IncidentFace = edges[edge];
                edges[edge].OuterComponent = processedEdges[new Edge(edge.p2, edge.p1)];
            }

        }

        //Set next/prev pointers
        foreach (KeyValuePair<Position, List<HalfEdge>> kvp in edgeDict2)
        {
            List<HalfEdge> list = kvp.Value;
            //Go through halfedges in clockwise order (highest other vertex, rightmost other vertex that is not highest, and the one thats left)
            HalfEdge he1 = list[0];
            foreach (HalfEdge he in list)
                if (he.Twin.Origin.position.y > he1.Twin.Origin.position.y)
                    he1 = he;
            list.Remove(he1);

            HalfEdge he2 = list[0];
            foreach (HalfEdge he in list)
                if (he.Twin.Origin.position.x > he2.Twin.Origin.position.x)
                    he2 = he;
            list.Remove(he2);

            //Ok that was overly complicated, but I challenge you to find a better way.
            //Now we can assign next and prev pointers

            if (list.Count == 0)
            {
                he1.Twin.Next = he2;
                he2.Prev = he1.Twin;

                he2.Twin.Next = he1;
                he1.Prev = he2.Twin;
            }

            else
            {
                HalfEdge he3 = list[0];


                he1.Twin.Next = he2;
                he2.Prev = he1.Twin;

                he2.Twin.Next = he3;
                he3.Prev = he2.Twin;

                he3.Twin.Next = he1;
                he1.Prev = he3.Twin;
            }


        }


    }


}


//The following objects are temporary, to hold code that can be added to the actual objects

/// <summary>
/// Vertex in the DCEL, represents a crossing (point where a town or city can be built)
/// </summary>
public class Vertex
{
	public Crossing crossing;

	public Position position;

	public Vertex(Position position)
	{
		this.position = position;
	}

	private HalfEdge incedentEdge;
	public HalfEdge IncidentEdge
	{
		get { return incedentEdge; }
		set { incedentEdge = value; }
	}

	/// <summary>
	/// The vertex neighbours. Only computes the neighbours once.
	/// </summary>
	private List<Vertex> vertexNeighbours;
    public List<Vertex> VertexNeighbours
    {
        get
        {
            if (vertexNeighbours == null)
                vertexNeighbours = _VertexNeighbours();
            return vertexNeighbours;
        }
    }

	/// <summary>
	/// Gives the three vertexes that are one edge away from this vertex.
	/// </summary>
	/// <returns>The neighbours.</returns>
	public List<Vertex> _VertexNeighbours()
	{
		List<Vertex> list = new List<Vertex> ();
		list.Add (incedentEdge.Twin.Origin);
		list.Add (incedentEdge.Prev.Origin);
		list.Add (incedentEdge.Twin.Next.Twin.Origin);
		return list;
	}


    /// <summary>
    /// Returns a list of half edges leading away from the vertex.
    /// </summary>
    /// <returns></returns>
    public List<HalfEdge> adjecentHalfEdges()
    {
        List<HalfEdge> list = new List<HalfEdge>();
        list.Add(incedentEdge);
        list.Add(incedentEdge.Prev.Twin);
        list.Add(incedentEdge.Twin.Next);
        return list;
    }

    internal IEnumerable<Face> adjecentFaces()
    {
        List<Face> list = new List<Face>();
        list.Add(incedentEdge.IncidentFace);
        list.Add(incedentEdge.Twin.IncidentFace);
        list.Add(incedentEdge.Prev.Twin.IncidentFace);

        return list;
    }
}

/// <summary>
/// Face. A face represents a tile
/// </summary>
public class Face
{
	public Tile tile;
	public Face()
	{

	}

	private HalfEdge outerComponent;
	public HalfEdge OuterComponent
	{
		get { return outerComponent;}
		set { outerComponent = value;}
	}

	//In this case the DCEL does not have inner components so no pointers are needed

	public List<HalfEdge> edges()
	{
		List<HalfEdge> list = new List<HalfEdge> ();
		list.Add (outerComponent);
		list.Add (outerComponent.Next);
		list.Add (outerComponent.Next.Next);
		list.Add (outerComponent.Next.Next.Next);
		list.Add (outerComponent.Next.Next.Next.Next);
		list.Add (outerComponent.Next.Next.Next.Next.Next);
		return list;
	}

	public List<Vertex> vertices()
	{
		List<Vertex> list = new List<Vertex> ();
		foreach (HalfEdge he in edges ())
			list.Add (he.Origin);
		return list;

	}

    public List<Face> faces()
    {
        List<Face> list = new List<Face>();
        foreach (HalfEdge he in edges())
            list.Add(he.Twin.IncidentFace);
        return list;
    }


}

/// <summary>
/// Half edge. A half edge and its twin represent a road between crossings.
/// </summary>
public class HalfEdge
{
	public Road road;

	public HalfEdge(Vertex origin)
	{
		this.origin = origin;
	}

	private Vertex origin;
	public Vertex Origin 
	{
		get { return origin;}
		set { origin = value;}
	}

	private HalfEdge twin;
	public HalfEdge Twin 
	{
		get { return twin;}
		set { twin = value;}
	}

	private Face incidentFace;
	public Face IncidentFace 
	{
		get { return incidentFace;}
		set { incidentFace = value;}
	}

	private HalfEdge next;
	public HalfEdge Next
	{
		get { return next;}
		set { next = value;}
	}
	
	private HalfEdge prev;
	public HalfEdge Prev {
		get { return prev;}
		set { prev = value;}
	}



    internal IEnumerable<Face> faces()
    {
        List<Face> list = new List<Face>();
        list.Add(incidentFace);
        list.Add(twin.incidentFace);
        return list;
    }

    internal IEnumerable<Vertex> adjecentVerteces()
    {
        List<Vertex> list = new List<Vertex>();
        list.Add(origin);
        list.Add(twin.origin);
        return list;
    }

    /// <summary>
    /// Generates a list of adjecent halfedges.
    /// NOTE: this might lead to a list with two halfedges of the same edge, if the edge is on the edge of the board.
    /// </summary>
    /// <returns></returns>
    internal IEnumerable<HalfEdge> adjecentHalfEdges()
    {
        List<HalfEdge> list = new List<HalfEdge>();
        list.Add(prev);
        list.Add(next);
        list.Add(twin.prev);
        list.Add(twin.next);
        return list;
    }
}

public struct Edge
{
	public Position p1;
	public Position p2;

	public Edge(Position p1, Position p2)
	{
		this.p1 = p1;
		this.p2 = p2;
	}
}

public class EdgeComparer : IEqualityComparer<Edge>
{
	public bool Equals(Edge e1, Edge e2)
	{
		return (Mathf.Round(e1.p1.x) == Mathf.Round(e2.p1.x)) && (Mathf.Round(e1.p1.y) == Mathf.Round(e2.p1.y)) && (Mathf.Round(e1.p2.x) == Mathf.Round(e2.p2.x)) && (Mathf.Round(e1.p2.y) == Mathf.Round(e2.p2.y));
	}
	
	public int GetHashCode(Edge e)
	{
		float i = 0f;
		if(e.p1.x > 0)
		    i = 1000000;

		float j = 0f;
		if (e.p1.y > 0)
			j = 100f;

		float test = Mathf.Round(i+(e.p1.x * 10000f) + ((Mathf.Abs (e.p1.y)) * 10f) + j);

		i = 0f;
		if(e.p2.x > 0)
		    i = 1000000;
		j = 0f;

		if (e.p2.y > 0)
			j = 100f;
		float test2 = Mathf.Round(i+(e.p2.x * 10000f) + ((Mathf.Abs (e.p2.y)) * 10f) + j);
		return (int) (test * 10000000 + test2);
	}
}

public struct Position
{
	public float x,y;
	public Position(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public string print()
	{
		return x + "," + y;
	}



}

public class PositionComparer : IEqualityComparer<Position>
{
	public bool Equals(Position p1, Position p2)
	{
		return ((Mathf.Round(p1.x) == Mathf.Round(p2.x)) && (Mathf.Round(p1.y) == Mathf.Round(p2.y)));
	}

	public int GetHashCode(Position p)
	{
		float i = 0f;
		if(p.x > 0)
		   i = 1000000;
		float j = 0f;
		if (p.y > 0)
			j = 100f;
		float test = Mathf.Round(j+(p.x * 10000f) + ((Mathf.Abs (p.y)) * 10f) + j);
		return (int)test;
	}
}
