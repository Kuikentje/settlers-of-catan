using UnityEngine;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

class GameState : MonoBehaviour
{
    //SETTINGS
    private const bool INCLUDEPLAYER = true;
    private const bool FORCEROBBER = false;

    //State bools (What is player currently doing)
    //This would be way prettier if we had just one enum with all the states or something.
    private bool inProgress;                //Game is in progress
    private bool boardButtons = false;      //Display buttons for actions available to current player (build road, town, city, buy card, play card)
    public bool roadHover = false;          //Building a road
    public bool townHover = false;          //Building a town
    private bool diceRollButton = false;    //Show dice roll button
    public bool monopoly = false;           //Playing a monopoly card
    bool trading = false, bank = false;     //Trading
    public bool tradeInProgres = false;     //Trade in progress (waiting for response)
    public bool moveRobber = false;         //Moving the robber
    public bool robTown = false;            //Robbing a town
    public bool buildCity = false;          //Building a city

    //Players
    private Dictionary<int, object> players;
    public Human humanPlayer;
    private Stack<Color> colors = new Stack<Color>(new Color[4] { Color.black, Color.green, Color.yellow, Color.red });

    //Turns
    private int lastRoll = 0;
    private GameObject scoreGuiGameObject;
    public TurnKeeper turnKeeper;

    //Board
    private Board board;
    public List<Road> buildableRoads;
    public List<Crossing> buildableTowns;
    public List<Crossing> buildableCities;
    private Stack<Card> cards = new Stack<Card>();

    //Other stuff
    public string warningLabel = "";
    public int freeRoads = 0;
    public int invention = 0;
    
    //Trading
    private ResourceType recieve, give;
    Vector2 tradeoptionsanchor = new Vector2(175, 0);
    int tradingPlayer = -1;
    TradeProposal tradeProposal;
    
    //Victory conditions
    public int playerWithLongestRoad = -1, lenghtLongestRoad = 0, playerWithMostKnights = -1, mostKnights = 0;
    private int neededVictoryPoints = 10;
    
    //Robber
    public ResourceTile robberLocation = null;

    //Stopwatch
    public Stopwatch stopwatch;

    //Make GameState a singleton class so only one gamestate exists
    private static GameState instance;
    private GameState() { }
    public static GameState Instance
    {
        get
        {
            //if (instance == null)
            //{
            //    GameObject go = new GameObject();
            //    instance = go.AddComponent<GameState>();
            //}
            return instance;
        }
    }

    void Start()
    {
        stopwatch = Stopwatch.StartNew();
        instance = gameObject.GetComponent<GameState>();
        StartGame();
    }

    void StartGame()
    {
        CreateCardStack();
        players = InitializePlayers();
        turnKeeper = new TurnKeeper(1, 1);
        board = gameObject.AddComponent<Board>();
        initializeGUI();
        inProgress = true;
        doTurn();
    }

    public void doTurn()
    {
        warningLabel = "";
        lastRoll = 0;
        if (GetPlayer(turnKeeper.getCurrentPlayer()).playerType == PlayerType.HUMAN)
        {
            Human player = (Human)GetPlayer(turnKeeper.getCurrentPlayer());
            player.DoTurn();
        }
        else if (GetPlayer(turnKeeper.getCurrentPlayer()).playerType == PlayerType.AGENT)
        {
            Agent player = (Agent)GetPlayer(turnKeeper.getCurrentPlayer());
            player.DoTurn();
        }
    }

    Dictionary<int, object> InitializePlayers()
    {
        System.Random random = new System.Random();
        int human;
        if (INCLUDEPLAYER)
        {
            human = random.Next(1, 5);
            //human = 4;
        }
        else human = -1;
        Dictionary<int, object> players = new Dictionary<int, object>();
        for (int i = 1; i < 5; i++)
        {
            if (human == i)
            {
                players.Add(i, new Human(i, colors.Pop()));
                humanPlayer = (Human)(Player)players[i];
            }
            else
            {
                players.Add(i, new Agent(i, colors.Pop()));
            }
        }

        return players;
    }

    void CreateCardStack()
    {
        //creating the deck
        Card[] deck = new Card[25];
        for (int i = 0; i < 25; i++)
            deck[i] = new Card();

        for (int i = 0; i < 14; i++)
            deck[i].category = "Knight";

        for (int i = 14; i < 20; i++)
            deck[i].category = "Development";

        for (int i = 20; i < 25; i++)
            deck[i].category = "Building";

        deck[14].name = "Invention";
        deck[15].name = "Invention";
        deck[16].name = "Roadconstruction";
        deck[17].name = "Roadconstruction";
        deck[18].name = "Monopoly";
        deck[19].name = "Monopoly";
        deck[20].name = "Library";
        deck[21].name = "University";
        deck[22].name = "Cathedral";
        deck[23].name = "Market";
        deck[24].name = "Parlement";

        //shuffeling the deck with Fisher-Yates
        for (int n = deck.Length - 1; n > 0; --n)
        {
            int k = Random.Range(0, 25);
            Card temp = deck[n];
            deck[n] = deck[k];
            deck[k] = temp;
        }
        // pusing the deck on the stack
        for (int i = 0; i < 25; i++)
        {
            cards.Push(deck[i]);
        }
    }


    /// <summary>
    /// initializes the gui
    /// </summary>
    public void initializeGUI()
    {
        GameObject resourceGUI = new GameObject();
        Rect rect = new Rect(0, 0, 200, 50);
        resourceGUI.transform.position = new Vector3(0.01f, 0.97f, 0.0f);
        resourceGUI.AddComponent<GUIText>();
        resourceGUI.GetComponent<GUIText>().color = Color.black;
        resourceGUI.GetComponent<GUIText>().richText = true;
        scoreGuiGameObject = resourceGUI;
    }

    /// <summary>
    /// roles dice and gives players the resources
    /// </summary>
    /// <returns></returns>
    public int RollDice()
    {
        int dice1 = Random.Range(1, 7);
        int dice2 = Random.Range(1, 7);
        int diceRoll = dice1 + dice2;
        if (FORCEROBBER && this.GetPlayer(GetCurrentPlayerID()).playerType == PlayerType.HUMAN)
        {
            diceRoll = 7;
        }
        lastRoll = diceRoll;
        distributeDiceRollResources(diceRoll);
        
        if (diceRoll == 7)
        {
            foreach(KeyValuePair<int, object> playerPair in GetPlayers())
            {
                Player player = (Player)playerPair.Value;
                if(player.playerType == PlayerType.HUMAN)
                {
                    Human human = (Human)player;
                    if(human.resources.Sum(x => x.Value) > 7)
                    {
                        if (this.GetPlayer(GetCurrentPlayerID()).playerType == PlayerType.HUMAN)
                        {
                            human.throwAwayResources = true;
                        }
                    }
                }
                else if(player.playerType == PlayerType.AGENT)
                {
                    Agent agent = (Agent)player;
                    agent.throwAwayResources();
                }
            }
            warningLabel = "move the robber";
        }
        return diceRoll;
    }
    public Board GetBoard() { return board; }
    public int GetRound() { return turnKeeper.getCurrentRound(); }

    /// <summary>
    /// build on the given edge/crossing if this player can/may do that. return true is succesfull, false if not possible
    /// </summary>
    /// <param name="playerId"></param>
    public bool CheckWinningConditions(int player)
    {
        // towns and cities
        int points = board.GetPoints(player);
        if (playerWithLongestRoad == player)
            points += 2;
        if (playerWithMostKnights == player)
            points += 2;

        points += GetPlayer(player).GetNumberOfBuildings();
        warningLabel = points.ToString();

        return points >= neededVictoryPoints;
    }
    public bool BuildRoad(Road r, int player)
    {
        r.placeRoad();
        GetPlayer(player).roadCount++;
        emptyListsAndLabels();
        if (isAllowedToBuildRoad(GetPlayer(player), freeRoads > 0))
        {
            initializeBuyableRoads();
        }
        //check longest road
        int l = board.LengthLongestRoad(player);
        if (l > 4 && (playerWithLongestRoad == -1 || l > lenghtLongestRoad))
        {
            lenghtLongestRoad = l;
            playerWithLongestRoad = player;
        }

        return true;
    }

    public bool BuildTown(Crossing c, int player)
    {
        c.placeTown();
        GetPlayer(player).townCount++;
        List<HarbourTile> harbours=  c.adjecentHarbourTiles();
        foreach (HarbourTile h in harbours)
            GetPlayer(player).ports[(ResourceType)h.type] = true;

        emptyListsAndLabels();
        if (isAllowedToBuildTown(GetPlayer(player)))
        {
            initializeBuyableTowns();
        }
        return true;
    }

    public bool BuildCity(Crossing c, int player)
    {
        c.placeCity();
        emptyListsAndLabels();
        return true; 
    }

    public int getHumanPlayer()
    {
        int p = 0;
        foreach(KeyValuePair<int, object> playerPair in GetPlayers())
        {
            Player player = (Player)playerPair.Value;
            if(player.playerType == PlayerType.HUMAN)
            {
                p = player.playerID;
            }
        }
        return p;
    }

    public Card GetCard() { return cards.Pop(); }
    public Player GetPlayer(int pid) { return (Player)players[pid]; }
    public int GetCurrentPlayerID() { return turnKeeper.getCurrentPlayer(); }
    public Dictionary<int, object> GetPlayers() { return players; }
    public void PlayMonopoly(ResourceType resource, int PlayerID)
    {
        foreach (KeyValuePair<int, object> p in players)
        {
            if (p.Key != PlayerID)
            {
                Player pl = (Player)p.Value;
                GetPlayer(PlayerID).resources[resource] += pl.resources[resource];
                pl.resources[resource] = 0;
            }
        }
    }

    public void Update()
    {
        if (inProgress)
        {
            if (scoreGuiGameObject)
            {
                string text = "";
                foreach (KeyValuePair<int, object> p in GetPlayers())
                {
                    Player player = (Player)p.Value;
                    string colorString = Util.rgbToHex(player.playerColor);
                    text += "<color=" + colorString + "><b>Player " + player.playerID + ":</b></color>\n";
                    if (player.playerType == PlayerType.HUMAN)
                    {
                        foreach (KeyValuePair<ResourceType, int> resource in player.resources)
                        {
                            text += "  " + Util.getResourceDescription(resource.Key) + ": " + resource.Value + "\n";
                        }
                        text += "  Knights: " + player.knights + "\n";
                    }
                    else
                    {
                        text += "  Total resources: " + player.resources.Sum(x => x.Value) + "\n  Knights: " + player.knights + "\n";
                    }
                }
                scoreGuiGameObject.GetComponent<GUIText>().text = text;
            }
        }
    }

    public void distributeDiceRollResources(int roll)
    {
        List<Crossing> crossings = board.crossings;
        List<ResourceTile> tiles = board.getTilesByNumber(roll);
        foreach (ResourceTile tile in tiles)
        {
            foreach (Crossing c in tile.Crossings)
            {
                Crossing crossing = crossings.Find(x => (x.vertex.position.x == c.vertex.position.x && x.vertex.position.y == c.vertex.position.y));
                if (crossing.playerID != 0 && !tile.robber)
                {
                    if (crossing.isCity)
                        GetPlayer(crossing.playerID).resources[tile.resource.resourceType] += 2;
                    else
                        GetPlayer(crossing.playerID).resources[tile.resource.resourceType]++;
                }
            }
        }
    }

    public void endTurn()
    {
        emptyListsAndLabels();
        turnKeeper.nextPlayer();
        doTurn();
    }

    void OnGUI()
    {
        if (humanPlayer != null)
            humanPlayer.OnGUI();

        if (lastRoll > 0)
        {
            GUIStyle style = new GUIStyle();
            style.richText = true;
            style.fontSize = 16;
            GUI.Label(new Rect(10, 400, 80, 20), "<b><color=" + Util.rgbToHex(GetPlayer(GetCurrentPlayerID()).playerColor) + ">Last dice roll: " + lastRoll + "</color></b>", style);
        }

        if (warningLabel != "")
        {
            GUIStyle style = new GUIStyle();
            style.richText = true;
            style.wordWrap = true;
            style.fontSize = 16;
            GUI.Label(new Rect(10, 420, 150, 20), "<color=red>" + warningLabel + "</color>", style);
        }
        if (getHumanPlayer() != 0)
        {
            Human human = (Human)GetPlayer(getHumanPlayer());
            if (human.throwAwayResources)
            {
                if (GUI.Button(new Rect(200, 600, 80, 20), "Lose Wool"))
                {
                    human.throwAwayResource(ResourceType.WOOL);
                }
                if (GUI.Button(new Rect(280, 600, 80, 20), "Lose Ore"))
                {
                    human.throwAwayResource(ResourceType.ORE);
                }
                if (GUI.Button(new Rect(360, 600, 80, 20), "Lose Stone"))
                {
                    human.throwAwayResource(ResourceType.STONE);
                }
                if (GUI.Button(new Rect(440, 600, 80, 20), "Lose Wood"))
                {
                    human.throwAwayResource(ResourceType.WOOD);
                }
                if (GUI.Button(new Rect(520, 600, 80, 20), "Lose Wheat"))
                {
                    human.throwAwayResource(ResourceType.WHEAT);
                }
                return;
            }
        }

        if (diceRollButton)
        {
            if (GUI.Button(new Rect(10, 450, 80, 20), "Roll Dice"))
            {
                warningLabel = "";
                Human human = (Human)GetPlayer(GetCurrentPlayerID());
                RollDice();
                diceRollButton = false;
                human.DiceRolled();
            }
        }

        
        
        if (boardButtons)
        {
            if (GUI.Button(new Rect(10, 500, 80, 20), "Buy Road"))
            {
                emptyListsAndLabels();
                initializeBuyableRoads();
            }
            if (GUI.Button(new Rect(10, 530, 80, 20), "Buy Town"))
            {
                emptyListsAndLabels();
                initializeBuyableTowns();
            }
                if (turnKeeper.getCurrentRound()>1)
            if (GUI.Button(new Rect(100, 500, 80, 20), "Buy City"))
            {
                buildCity = true;
                emptyListsAndLabels();
                initializeBuyableCities();
            }
            if (GUI.Button(new Rect(100, 530, 80, 20), "End Turn"))
            {
                Human human = (Human)GetPlayer(GetCurrentPlayerID());
                if (GetRound() == 1)
                {
                    if (human.currentRoundRoadCount < 1 || human.currentRoundTownCount < 1)
                    {
                        return;
                    }
                }
                warningLabel = "";
                boardButtons = false;
                human.endTurn();
            }
            if (turnKeeper.getCurrentRound() > 1)
                if (GUI.Button(new Rect(190, 500, 80, 20), "Buy Card"))
                {
                    emptyListsAndLabels();
                    Player player = GetPlayer(GetCurrentPlayerID());
                    if (player.resources[ResourceType.ORE] > 0 && player.resources[ResourceType.WOOL] > 0 && player.resources[ResourceType.WHEAT] > 0)
                        player.GetCard();
                    else
                        warningLabel = "You do not have enough resources to buy a card";
                }
            ShowCards();
            ShowTradeOptions();
        }
        
        if (trading)
        {
            GUIStyle style = new GUIStyle();
            style.richText = true;
            style.wordWrap = true;
            style.fontSize = 16;
            Player player = GetPlayer(GetCurrentPlayerID());
            if (bank)
            {
                int amount = player.GetHarborRate(give);
                GUI.Label(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 180, 200, 20), "<color=green>Trade " + amount + " " + give + " for 1 " + recieve + "?</color>", style);
            }
            else if (tradingPlayer != -1)
            {
                GUI.Label(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 180, 200, 20), "<color=green>Select the resources and press trade to trade</color>", style);
            }
            else if (trading)
            {
                GUI.Label(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 180, 200, 20), "<color=green>Select player or bank to trade with</color>", style);
            }
        }
    }


    private void ShowTradeOptions()
    {
        if (!trading)
        {
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30, 60, 20), "Trade"))
                trading = true;
            return;
        }


        if (tradingPlayer != -1)
        {

            // trade with player
            int i = 1;
            GUI.Label(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y, 150, 20), "recieve          give");
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Wood:" + tradeProposal.receiving[ResourceType.WOOD]))
                tradeProposal.receiving[ResourceType.WOOD]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 65, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.receiving[ResourceType.WOOD]--;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 90, tradeoptionsanchor.y + 30 * i, 60, 20), "Wood:" + tradeProposal.sending[ResourceType.WOOD]))
                tradeProposal.sending[ResourceType.WOOD]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 155, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.sending[ResourceType.WOOD]--;
            i++;

            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Ore:" + tradeProposal.receiving[ResourceType.ORE]))
                tradeProposal.receiving[ResourceType.ORE]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 65, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.receiving[ResourceType.ORE]--;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 90, tradeoptionsanchor.y + 30 * i, 60, 20), "Ore:" + tradeProposal.sending[ResourceType.ORE]))
                tradeProposal.sending[ResourceType.ORE]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 155, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.sending[ResourceType.ORE]--;
            i++;

            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Stone:" + tradeProposal.receiving[ResourceType.STONE]))
                tradeProposal.receiving[ResourceType.STONE]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 65, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.receiving[ResourceType.STONE]--;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 90, tradeoptionsanchor.y + 30 * i, 60, 20), "Stone:" + tradeProposal.sending[ResourceType.STONE]))
                tradeProposal.sending[ResourceType.STONE]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 155, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.sending[ResourceType.STONE]--;
            i++;

            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Wool:" + tradeProposal.receiving[ResourceType.WOOL]))
                tradeProposal.receiving[ResourceType.WOOL]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 65, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.receiving[ResourceType.WOOL]--;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 90, tradeoptionsanchor.y + 30 * i, 60, 20), "Wool:" + tradeProposal.sending[ResourceType.WOOL]))
                tradeProposal.sending[ResourceType.WOOL]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 155, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.sending[ResourceType.WOOL]--;
            i++;

            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Wheat:" + tradeProposal.receiving[ResourceType.WHEAT]))
                tradeProposal.receiving[ResourceType.WHEAT]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 65, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.receiving[ResourceType.WHEAT]--;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 90, tradeoptionsanchor.y + 30 * i, 60, 20), "Wheat:" + tradeProposal.sending[ResourceType.WHEAT]))
                tradeProposal.sending[ResourceType.WHEAT]++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 155, tradeoptionsanchor.y + 30 * i, 20, 20), "-"))
                tradeProposal.sending[ResourceType.WHEAT]--;
            i++;


            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 220, 60, 20), "Trade"))
            {
                if (tradeProposal.otherPlayer.AcceptTrade(tradeProposal))
                {
                    tradeProposal.PerformTrade();
                    trading = false;
                    tradingPlayer = -1;
                }
            }
        }

        else if (bank)
        {
            int i = 1;
            GUI.Label(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y, 150, 20), "recieve          give");
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Wood"))
                recieve = ResourceType.WOOD;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 80, tradeoptionsanchor.y + 30 * i, 60, 20), "Wood"))
                give = ResourceType.WOOD;
            i++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Ore"))
                recieve = ResourceType.ORE;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 80, tradeoptionsanchor.y + 30 * i, 60, 20), "Ore"))
                give = ResourceType.ORE;
            i++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Stone"))
                recieve = ResourceType.STONE;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 80, tradeoptionsanchor.y + 30 * i, 60, 20), "Stone"))
                give = ResourceType.STONE;
            i++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Wool"))
                recieve = ResourceType.WOOL;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 80, tradeoptionsanchor.y + 30 * i, 60, 20), "Wool"))
                give = ResourceType.WOOL;
            i++;
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), "Wheat"))
                recieve = ResourceType.WHEAT;
            if (GUI.Button(new Rect(tradeoptionsanchor.x + 80, tradeoptionsanchor.y + 30 * i, 60, 20), "Wheat"))
                give = ResourceType.WHEAT;

            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 220, 60, 20), "Trade"))
            {
                Player player = GetPlayer(GetCurrentPlayerID());
                int amount = player.GetHarborRate(give);
                if (player.resources[give] >= amount)
                {
                    player.resources[give] -= amount;
                    player.resources[recieve] += 1;
                }
                trading = false;
                bank = false;
            }
        }


        else
        {
            if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30, 60, 20), "bank"))
                bank = true;
            int i = 2;
            foreach (KeyValuePair<int, object> p in players)
            {
                if (GUI.Button(new Rect(tradeoptionsanchor.x, tradeoptionsanchor.y + 30 * i, 60, 20), p.Key.ToString()))
                {
                    tradeProposal = new TradeProposal(GetPlayer(GetCurrentPlayerID()), GetPlayer(p.Key));
                    tradingPlayer = p.Key;
                }
                i++;
            }
        }
    }

    private void ShowCards()
    {
        Player player = GetPlayer(GetCurrentPlayerID());
        if (player.cards.Count > 0)
        {
            int i = 0;
            foreach (Card c in player.cards)
            {

                string text = c.category + ": \n" + c.name;
                if (GUI.Button(new Rect(1400, 15 + i * 60, 100, 40), text))
                {
                    if (c.name == "Roadconstruction")
                    {
                        emptyListsAndLabels();
                        initializeBuyableRoads(true);
                        freeRoads = 2;
                        warningLabel = "Place two free roads";
                        player.cards.Remove(c);
                    }
                    if (c.category == "Knight")
                    {
                        player.PlayCard(c, new PlayCardData());
                        player.cards.Remove(c);
                        warningLabel = "Move the robber";
                        moveRobber = true;
                    }
                    if (c.name == "Invention")
                    {
                        invention = 2;
                        player.cards.Remove(c);
                    }
                    if (c.name == "Monopoly")
                    {
                        monopoly = true;
                        player.cards.Remove(c);
                    }

                }
                i++;
            }

        }
        if (monopoly)
        {
            warningLabel = "pick a resource to play monopoly on";
            if (GUI.Button(new Rect(1700, 15, 100, 20), "Wheat"))
            {
                PlayMonopoly(ResourceType.WHEAT, player.playerID);
                monopoly = false;
            }
            if (GUI.Button(new Rect(1700, 35, 100, 20), "Wool"))
            {
                PlayMonopoly(ResourceType.WOOL, player.playerID);
                monopoly = false;
            }
            if (GUI.Button(new Rect(1700, 55, 100, 20), "Ore"))
            {

                PlayMonopoly(ResourceType.ORE, player.playerID);
                monopoly = false;
            }
            if (GUI.Button(new Rect(1700, 75, 100, 20), "Stone"))
            {
                PlayMonopoly(ResourceType.STONE, player.playerID);
                monopoly = false;
            }
            if (GUI.Button(new Rect(1700, 95, 100, 20), "Wood"))
            {
                PlayMonopoly(ResourceType.WOOD, player.playerID);
                monopoly = false;
            }
        }

        if (invention > 0)
        {
            warningLabel = "pick two free resources";
            if (GUI.Button(new Rect(1600, 15, 100, 20), "Wheat"))
            {
                player.resources[ResourceType.WHEAT]++;
                invention--;
            }
            if (GUI.Button(new Rect(1600, 35, 100, 20), "Wool"))
            {
                player.resources[ResourceType.WOOL]++;
                invention--;
            }
            if (GUI.Button(new Rect(1600, 55, 100, 20), "Ore"))
            {
                player.resources[ResourceType.ORE]++;
                invention--;
            }
            if (GUI.Button(new Rect(1600, 75, 100, 20), "Stone"))
            {
                player.resources[ResourceType.STONE]++;
                invention--;
            }
            if (GUI.Button(new Rect(1600, 95, 100, 20), "Wood"))
            {
                player.resources[ResourceType.WOOD]++;
                invention--;
            }
        }


    }

    private void emptyListsAndLabels()
    {
        warningLabel = "";
        roadHover = false;
        townHover = false;
        buildableTowns = new List<Crossing>();
        buildableCities = new List<Crossing>();
        buildableRoads = new List<Road>();
    }

    private void initializeBuyableRoads(bool free = false)
    {
        Player player = GetPlayer(GetCurrentPlayerID());
        if (!isAllowedToBuildRoad(player, free))
        {
            warningLabel = "You are not allowed to build a new road";
            return;
        }

        roadHover = true;
        buildableRoads = board.validRoadPlacement();
    }

    private void initializeBuyableTowns()
    {
        Player player = GetPlayer(GetCurrentPlayerID());
        if (!isAllowedToBuildTown(player))
        {
            warningLabel = "You are not allowed to build a new town";
            return;
        }
        townHover = true;
        buildableTowns = board.validTownPlacement();
    }

    private void initializeBuyableCities()
    {
        Player player = GetPlayer(GetCurrentPlayerID());
        if (player.resources[ResourceType.WHEAT] <= 1 || player.resources[ResourceType.ORE] <= 2)
        {
            warningLabel = "You do not have enough resources to build a city";
            return;
        }
    }

    public bool isAllowedToBuildTown(Player p)
    {
        int round = turnKeeper.getCurrentRound();
        int playerCount = GetPlayers().Count;


        if (turnKeeper.getCurrentRound() == 1)
        {
            if (p.currentRoundTownCount < 1)
            {
                return true;
            }
        }
        else
        {
            if ((p.resources[ResourceType.WOOD] > 0 && p.resources[ResourceType.STONE] > 0 && p.resources[ResourceType.WOOL] > 0 && p.resources[ResourceType.WHEAT] > 0))
            {
                return true;
            }
        }
        return false;
    }

    public bool isAllowedToBuildRoad(Player p, bool free = false)
    {
        int round = turnKeeper.getCurrentRound();
        int playerCount = GetPlayers().Count;

        if (free)
            return true;

        if (turnKeeper.getCurrentRound() == 1)
        {
            if (p.currentRoundRoadCount < 1)
            {
                return true;
            }
        }
        else
        {
            if (p.resources[ResourceType.WOOD] > 0 && p.resources[ResourceType.STONE] > 0)
            {
                return true;
            }
        }
        return false;
    }

    public void initializeBoardUI()
    {
        boardButtons = true;
    }

    public void initializeDiceUI()
    {
        diceRollButton = true;
    }
    public void initializeUI()
    {
        if (GetRound() > 1)
        {
            initializeDiceUI();
        }
        else
        {
            initializeBoardUI();
        }
    }

    IEnumerator WaitForResponse()
    {
        while (tradeInProgres)
        {
            yield return null;

        }
        print(GetPlayer(GetCurrentPlayerID())+" ended his turn "+GetRound());
        GetPlayer(GetCurrentPlayerID()).endTurn();
    }

    public void Wait()
    {
        StartCoroutine("WaitForResponse");
    }

    public void WaitForThrowaway()
    {
        StartCoroutine("WaitForHumanThrowaway");
    }

    IEnumerator WaitForHumanThrowaway()
    {
        Human h = (Human)GetPlayer(getHumanPlayer());
        while(h.throwAwayResources)
        {
            yield return null;
        }
    }

    public void CheckKnights(int player)
    {
        int k = GetPlayer(player).knights;
        if (k > 2 && (playerWithMostKnights == -1 || k > mostKnights))
        {
            playerWithMostKnights = player;
            mostKnights = k;
        }
    }

    public void MoveRobber(ResourceTile t)
    {
        t.robber = true;
        if (robberLocation != null)
        {
            robberLocation.robber = false;
        }
        robberLocation = t;
        this.robTown = true;
        moveRobber = false;
        warningLabel = "Select the town you want to rob";
    }

    public void RobPlayer(int pid, Crossing c)
    {
        if (!robberLocation.Crossings.Contains(c))
            return;
        Player p = GetPlayer(pid);
        int r = p.resources.Sum(x => x.Value);
        System.Random random = new System.Random();
        int rnd = random.Next(1, r+1);
        int offset = 0;

        //string debugLog = "The random number is " + rnd + "\n";

        foreach (ResourceType resourceType in System.Enum.GetValues(typeof(ResourceType)))
        {   
            if(resourceType != ResourceType.ANY && resourceType != ResourceType.NONE)
            {
                //debugLog += "player has " + p.resources[resourceType] + " of resource " + Util.getResourceDescription(resourceType) + " the current offset is " + offset + "  and " + (offset + p.resources[resourceType]) + "\n";
                if(p.resources[resourceType] > 0)
                {
                    if(rnd <= (offset + p.resources[resourceType]) && rnd > offset)
                    {
                        p.resources[resourceType]--;
                        GetPlayer(GetCurrentPlayerID()).resources[resourceType]++;
                        robTown = false;
                        warningLabel = "";
                        break;
                    }
                    else
                    {
                        offset += p.resources[resourceType];
                    }
                }
                else
                {
                    continue;
                }
            }
        }
    }

    public void GiveInitialResources()
    {
        for (int i=2; i<13;i++)
            distributeDiceRollResources(i);
    }

    internal int getPlayerPoints(int player)
    {
        // towns and cities
        int points = board.GetPoints(player);
        if (playerWithLongestRoad == player)
            points += 2;
        if (playerWithMostKnights == player)
            points += 2;

        points += GetPlayer(player).GetNumberOfBuildings();

        return points;
    }
}
