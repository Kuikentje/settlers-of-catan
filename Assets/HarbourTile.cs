﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

public enum HarbourType
{
    [Description("Wool")]
    WOOL,
    [Description("Ore")]
    ORE,
    [Description("Stone")]
    STONE,
    [Description("Wheat")]
    WHEAT,
    [Description("Wood")]
    WOOD,
    [Description("3for1")]
    THREEFORONE
}

public class HarbourTile : Tile
{
    public HarbourType type = HarbourType.THREEFORONE;
    void Start()
    {
        GetComponent<Renderer>().material.color = Color.blue;
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        style.richText = true;
        style.fontSize = 20;

        var p = Camera.main.WorldToScreenPoint(transform.position);
        Rect rect = new Rect(0, 0, 100, 20);
        rect.center = new Vector2(p.x, Screen.height - p.y);
        GUI.Label(rect, "<color=white>"+GetEnumDescription(type)+"</color>", style);
    }

    public static string GetEnumDescription(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());

        DescriptionAttribute[] attributes =
            (DescriptionAttribute[])fi.GetCustomAttributes(
            typeof(DescriptionAttribute),
            false);

        if (attributes != null &&
            attributes.Length > 0)
            return attributes[0].Description;
        else
            return value.ToString();
    }
}
