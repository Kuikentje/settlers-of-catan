using UnityEngine;
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;

class Human : Player
{
    bool trading = false;

    public bool throwAwayResources = false;

    private GameState game;
    private bool diceRolled = false;
    private TradeProposal trade;


    public Human(int pid, Color color) : base(pid, color)
    {
        game = GameState.Instance;
        playerType = PlayerType.HUMAN;
    }

    public new void DoTurn()
    {
        game.initializeUI();
    }

    public new void endTurn()
    {
        
        base.endTurn();
    }

    public void DiceRolled()
    {
        diceRolled = true;
        game.initializeBoardUI();
    }

public void OnGUI()
{
    if (!trading) return;

    GUIStyle style = new GUIStyle();
    style.richText = true;
    style.wordWrap = true;
    style.fontSize = 16;
    string text = "Player " + trade.player.playerID + " proposes the following trade. You will be given: \n";
    foreach (KeyValuePair<ResourceType, uint> k in trade.sending)
        // if (k.Value>0)
        text += k.Value + " " + k.Key + "\n";
    text += " for:\n";
    foreach (KeyValuePair<ResourceType, uint> k in trade.receiving)
        //  if (k.Value > 0)
        text += k.Value + " " + k.Key + "\n";

    GUI.Label(new Rect(200, 10, 200, 20), "<color=green>" + text + "</color>", style);

    if (GUI.Button(new Rect(200, 500, 60, 20), "Accept"))
    {
        trade.PerformTrade();
        game.tradeInProgres = false;
        trading = false;
    }
    if (GUI.Button(new Rect(280, 500, 60, 20), "Decline"))
    {
        game.tradeInProgres = false;
        trading = false;
    }
}

    public override bool AcceptTrade(TradeProposal t)
    {
        trading = true;
        trade = t;           
        return true;
    }
}