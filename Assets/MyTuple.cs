﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


class MyTuple
{
    public int value;
    public ResourceType key;
    
    MyTuple(ResourceType s, int i)
    {
        value = i; key = s;
    }
}
