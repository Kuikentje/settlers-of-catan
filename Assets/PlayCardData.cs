﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayCardData
{
    public Road e1, e2;
    public ResourceType resource1, resource2;
}