using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TradeProposal
{
    public Dictionary<ResourceType, uint> receiving, sending;
    public Player player, otherPlayer;
    public bool counter = false;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="p">Player that sends proposal</param>
    /// <param name="q">Player that receives proposal</param>
    public TradeProposal(Player p, Player q)
    {
        player = p;
        otherPlayer = q;

        receiving = new Dictionary<ResourceType, uint>();
        receiving.Add(ResourceType.ORE, 0);
        receiving.Add(ResourceType.STONE, 0);
        receiving.Add(ResourceType.WHEAT, 0);
        receiving.Add(ResourceType.WOOD, 0);
        receiving.Add(ResourceType.WOOL, 0);

        sending = new Dictionary<ResourceType, uint>();
        sending.Add(ResourceType.ORE, 0);
        sending.Add(ResourceType.STONE, 0);
        sending.Add(ResourceType.WHEAT, 0);
        sending.Add(ResourceType.WOOD, 0);
        sending.Add(ResourceType.WOOL, 0);
    }

    public void PerformTrade()
    {
        foreach (KeyValuePair<ResourceType, uint> kvp in receiving)
            if (otherPlayer.resources[kvp.Key] < kvp.Value)
                return;
        foreach (KeyValuePair<ResourceType, uint> kvp in sending)
            if (player.resources[kvp.Key] < kvp.Value)
                return;

        foreach (KeyValuePair<ResourceType, uint> kvp in receiving)
        {
            otherPlayer.resources[kvp.Key] -= (int)kvp.Value;
            player.resources[kvp.Key] += (int)kvp.Value;
        }
        foreach (KeyValuePair<ResourceType, uint> kvp in sending)
        {
            player.resources[kvp.Key] -= (int)kvp.Value;
            otherPlayer.resources[kvp.Key] += (int)kvp.Value;
        }
    }

    public bool Equals(TradeProposal t)
    {
        if (player != t.player)
            return false;

        if (otherPlayer != t.otherPlayer)
            return false;

        foreach (KeyValuePair<ResourceType, uint> kvp in receiving)
            if (kvp.Value != t.receiving[kvp.Key])
                return false;

        foreach (KeyValuePair<ResourceType, uint> kvp in sending)
            if (kvp.Value != t.sending[kvp.Key])
                return false;
        return true;
    }

    public override bool Equals(System.Object obj)
    {
        // If parameter is null return false.
        if (obj == null)
        {
            return false;
        }

        // If parameter cannot be cast to Point return false.
        TradeProposal t = obj as TradeProposal;
        if ((System.Object)t == null)
        {
            return false;
        }

        return Equals(t);
    }

    public override int GetHashCode()
    {
        string s = player + "" + otherPlayer + receiving.GetHashCode() + sending.GetHashCode();
        return s.GetHashCode();
    }

    internal bool Empty()
    {
        foreach (int i in receiving.Values)
            if (i > 0)
                return true;
        foreach (int i in sending.Values)
            if (i > 0)
                return true;
        return false;
    }

    internal TradeProposal ReverseCopy()
    {
        TradeProposal copy = new TradeProposal(player, otherPlayer);
        foreach(ResourceType rt in receiving.Keys)
        {
            copy.receiving[rt] = sending[rt];
            copy.sending[rt] = receiving[rt];
        }
        return copy;
    }
}

public class Player
{
    public PlayerType playerType;
    private GameState game;
    public Color playerColor;
    public int playerID;
    public Dictionary<ResourceType, int> resources = new Dictionary<ResourceType,int>();
    public List<Card> cards;
    public int knights = 0;
    protected int Points = 0;
    public int townCount = 0;
    public int roadCount = 0;
    public int currentRoundTownCount = 0;
    public int currentRoundRoadCount = 0;
    public Dictionary<ResourceType, bool> ports;


    public Player(int id, Color color)
    {
        playerColor = color;
        playerID = id;
        cards = new List<Card>();
        game = GameState.Instance;
        resources.Add(ResourceType.WOOD, 0);
        resources.Add(ResourceType.ORE, 0);
        resources.Add(ResourceType.STONE, 0);
        resources.Add(ResourceType.WOOL, 0);
        resources.Add(ResourceType.WHEAT, 0);

        ports = new Dictionary<ResourceType, bool>();
        ports.Add(ResourceType.NONE, false);
        ports.Add(ResourceType.ORE, false);
        ports.Add(ResourceType.STONE, false);
        ports.Add(ResourceType.WHEAT, false);
        ports.Add(ResourceType.WOOD, false);
        ports.Add(ResourceType.WOOL, false);
    }

    public void DoTurn()
    {
        Debug.Log("ROUND :" + game.turnKeeper.getCurrentRound());

        Debug.Log(game.stopwatch.ElapsedMilliseconds / 1000f);
    }

    /// <summary>
    /// Tries building a road on edge e
    /// </summary>
    /// <param name="e">The edge</param>
    /// <returns>True if succesfull, false if unsuccesfull</returns>
    public bool BuildRoad(Road r, bool free = false)
    {
        if(game.isAllowedToBuildRoad(this, free))
        {
            if (free)
            {
                game.BuildRoad(r, playerID);
                game.freeRoads--;
                currentRoundTownCount++;
                return true;
            }

            if(game.GetRound() > 1)
            {
                if (game.BuildRoad(r, playerID))
                {
                    resources[ResourceType.WOOD] -= 1;
                    resources[ResourceType.STONE] -= 1;
                    currentRoundTownCount++;
                    return true;
                }
            }
            else
            {
                game.BuildRoad(r, playerID);
                currentRoundRoadCount++;
                return true;
            }
        }
        return false;
    }

    public bool throwAwayResource(ResourceType resource)
    {
        if(resources[resource] > 0)
        {
            resources[resource]--;
            if(playerType == PlayerType.HUMAN)
            {
                Human human = (Human)this;
                if (human.resources.Sum(x => x.Value) <= 7)
                {
                    human.throwAwayResources = false;
                }
            }
            
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Tries building a town on crossing c
    /// </summary>
    /// <param name="c">the crossing</param>
    /// <returns>True if succesfull, false if unsuccesfull</returns>
    public bool BuildTown(Crossing c)
    {
        if(game.isAllowedToBuildTown(this))
        {
            if(game.GetRound() > 1)
            {

                if (game.BuildTown(c, playerID))
                {
                    resources[ResourceType.WOOD] -= 1;
                    resources[ResourceType.WHEAT] -= 1;
                    resources[ResourceType.WOOL] -= 1;
                    resources[ResourceType.STONE] -= 1;
                    currentRoundTownCount++;
                    return true;
                }
            }
            else
            {
                game.BuildTown(c, playerID);
                currentRoundTownCount++;
                return true;
            }
        }
        return false;
    }

    public void endTurn()
    {
        currentRoundRoadCount = 0;
        currentRoundTownCount = 0;
        //after every turn check if player has won
        if (game.CheckWinningConditions(playerID))
            game.warningLabel = "Player "+playerID+" has won the game!";
        else
            game.endTurn();
    }

    /// <summary>
    /// Tries building a city on crossing c
    /// </summary>
    /// <param name="c">the crossing</param>
    /// <returns>True if succesfull, false if unsuccesfull</returns>
    public bool BuildCity(Crossing c)
    {
        if (resources[ResourceType.WHEAT] > 1 && resources[ResourceType.ORE] > 2)
            if (game.BuildCity(c, playerID))
            {

                resources[ResourceType.WHEAT] -= 2;
                resources[ResourceType.ORE] -= 3;
                return true;
            }
        return false;
    }

    public bool GetCard()
    {
        if (resources[ResourceType.ORE] > 0 && resources[ResourceType.WOOL] > 0 && resources[ResourceType.WHEAT] > 0)
        {
            resources[ResourceType.ORE] -= 1;
            resources[ResourceType.WOOL] -= 1;
            resources[ResourceType.WHEAT] -= 1;
            cards.Add(game.GetCard());
            return true;
        }
        return false;
    }

    public void PlayCard(Card card, PlayCardData data)
    {
        switch (card.category)
        {
            case "Development":
                switch (card.name)
                {
                    case "Invention":
                        resources[data.resource1]++;
                        resources[data.resource2]++;
                        break;
                    case "Roadconstruction":// done by gamestate
                        break;
                    case "Monopoly":
                        game.PlayMonopoly(data.resource1, playerID);
                        break;
                }
                break;
            case "Knight":
                knights++;
                game.CheckKnights(playerID);
                break;

            case "Building":
                // TODO create a card/ picture on the board
                Points++;
                break;
        }
    }

    /// <summary>
    /// Perform a trade action
    /// </summary>
    /// <param name="giving">list of MyTuples that represent the resources the player gives to the other player (there where no tuples :( ).</param>
    /// <param name="recieving">list of MyTuples that represent the resources the player recieves to the other player (there where no tuples :( ).</param>
    /// <param name="otherPlayer">id of the other player</param>
    /// <returns>true is succesfull</returns>
    bool PerformTrade(List<MyTuple> giving, List<MyTuple> recieving, int otherPlayer)
    {
        Player p = game.GetPlayer(otherPlayer);

        // check if possible
        foreach (MyTuple t in giving)
            if (resources[t.key] < t.value)
                return false;

        foreach (MyTuple t in recieving)
            if (p.resources[t.key] < t.value)
                return false;

        // the trade
        foreach (MyTuple t in giving)
        {
            resources[t.key] -= t.value;
            p.resources[t.key] += t.value;
        }
        foreach (MyTuple t in recieving)
        {
            resources[t.key] += t.value;
            p.resources[t.key] -= t.value;
        }

        return true;
    }
    
    public int GetHarborRate(ResourceType r)
    {
        if (ports[r])
            return 2;
        else if (ports[ResourceType.NONE])
            return 3;
        return 4;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual bool AcceptTrade(TradeProposal t) { game.tradeInProgres = false;return true;  }

    public int GetNumberOfBuildings()
    {
        int p = 0;
        foreach (Card c in cards)
            if (c.category == "Building")
                p++;
        return p;
    }

    #region heuristics

    /// <summary>
    /// Returns a value on how many rolls there will be end until end of game.
    /// </summary>
    public int PredictRollsLeft()
    {
        int bestscore = 2;
        foreach (int pid in game.GetPlayers().Keys)
        {
            int score = game.GetBoard().GetPoints(pid);
            if (score > bestscore)
                bestscore = score;
        }
        int playercount = game.GetPlayers().Count;
        return Mathf.Max(playercount, (8 - bestscore) * playercount * 2);
    }

    /// <summary>
    /// Returns the n/36 chance a tile will produce a resource
    /// </summary>
    public int ResourceNumberToValue(int n)
    {
        if (n > 7)
            return 13 - n;
        else if (n != 7)
            return n;
        else return 0;
    }

    /// <summary>
    /// Returns a score for a given ResourceTile, score can be compared with GetResourceCardValue.
    /// </summary>
    private int GetResourceTileValue(ResourceTile rt)
    {
        //TODO: should also give a score to harbors....
        //tile value is based on needs, harbors, chance, and rolls left
        int score = ResourceNumberToValue(rt.number);
        if (GetHarborRate(rt.resource.resourceType) == 2)
            score *= 2;
        score *= PredictRollsLeft();
        return score;
    }

    /// <summary>
    /// Returns a score for a given ResourceType, score can be compared with GetResourceTileValue.
    /// </summary>
    public int GetResourceCardValue(ResourceType rt)
    {
        //card value is based on needs, harbors
        int score = 36;
        if (GetHarborRate(rt) == 2)
            score *= 2;
        return score;
    }

    #endregion heuristics

}
