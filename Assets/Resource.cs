﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public abstract class Resource
{
    public abstract Color Color { get; }
    public abstract string Name { get; }
    public abstract ResourceType resourceType { get; }
}

public class Desert : Resource
{
    public override Color Color
    {
        get { return Color.yellow; }
    }

    public override string Name
    {
        get { return "Desert"; }
    }

    public override ResourceType resourceType
    {
        get { return ResourceType.NONE; }
    }
}

public class Wood : Resource
{
    public override Color Color
    {
        get { return new Color(85F / 255, 128F / 255, 48F / 255); }
    }

    public override string Name
    {
        get { return "Wood"; }
    }

    public override ResourceType resourceType
    {
        get { return ResourceType.WOOD; }
    }
}

public class Wool : Resource
{
    public override Color Color
    {
        get { return new Color(153F / 255, 177F / 255, 37F / 255); }
    }

    public override string Name
    {
        get { return "Wool"; }
    }

    public override ResourceType resourceType
    {
        get { return ResourceType.WOOL; }
    }
}

public class Ore : Resource
{
    public override Color Color
    {
        get { return new Color(147F / 255, 140F / 255, 148F / 255); }
    }

    public override string Name
    {
        get { return "Ore"; }
    }

    public override ResourceType resourceType
    {
        get { return ResourceType.ORE; }
    }
}

public class Stone : Resource
{
    public override Color Color
    {
        get { return new Color(216F / 255, 168F / 255, 83F / 255); }
    }

    public override string Name
    {
        get { return "Stone"; }
    }

    public override ResourceType resourceType
    {
        get { return ResourceType.STONE; }
    }
}

public class Wheat : Resource
{
    public override Color Color
    {
        get { return new Color(232F / 255, 193F / 255, 14F / 255); }
    }

    public override string Name
    {
        get { return "Wheat"; }
    }

    public override ResourceType resourceType
    {
        get { return ResourceType.WHEAT; }
    }
}

