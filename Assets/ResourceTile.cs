﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Any tile that gives a resource is a resource tile
/// </summary>
public class ResourceTile : Tile
{
    public Resource resource = new Wood();
    public int number = 7;
    public bool robber = false;

    void Start()
    {
        Board parent = transform.parent.GetComponent<Board>();

    }

    void Update()
    {
        GetComponent<Renderer>().material.color = resource.Color;
    }

    void OnMouseDown()
    {
        if (GameState.Instance.moveRobber)
            GameState.Instance.MoveRobber(this);
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        if (number == 6 || number == 8)
        {
            style.normal.textColor = Color.red;
            style.fontSize = 32;
        }
        if (number == 5 || number == 9)
            style.fontSize = 28;
        if (number == 4 || number == 10)
            style.fontSize = 24;
        if (number == 3 || number == 11)
            style.fontSize = 20;
        if (number == 2 || number == 12)
            style.fontSize = 16;

        var p = Camera.main.WorldToScreenPoint(transform.position);
        Rect rect = new Rect(0,0, 100, 20);
        rect.center = new Vector2(p.x,Screen.height - p.y);
        if (!robber)
            GUI.Label(rect, "" + number,style);
        else
            GUI.Label(rect, "Robber ("+number+")", style);
    }

}

