﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

public enum ResourceType
{
    [Description("Wool")] WOOL,
    [Description("Ore")] ORE,
    [Description("Stone")] STONE,
    [Description("Wheat")] WHEAT,
    [Description("Wood")] WOOD,
    [Description("Any")] ANY,
    [Description("None")] NONE
}