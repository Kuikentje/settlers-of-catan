﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Road : MonoBehaviour
{
    public HalfEdge halfEdge;
    public int playerID;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public bool placeRoad()
    {
        if (isValidPlacement())
        {
            playerID = GameState.Instance.GetCurrentPlayerID();
            GetComponent<Renderer>().material.color = GameState.Instance.GetPlayer(playerID).playerColor;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Checks if this road is a valid placement for a road of the current player
    /// </summary>
    /// <returns></returns>
    internal bool isValidPlacement()
    {
       //Check if there isn't already a road
        if (playerID != 0)
            return false;

        //Check if the road is on land
        bool resourceTile = false;
        foreach (Tile tile in adjecentTiles())
        {
            if (tile is ResourceTile)
                resourceTile = true;
        }
        if (!resourceTile)
            return false;

        //Check if there is another road or a town adjecent
        foreach (Crossing c in adjecentCrossings())
            if (c.playerID == GameState.Instance.GetCurrentPlayerID())
                return true;
        foreach (Road r in adjecentRoads())
            if (r.playerID == GameState.Instance.GetCurrentPlayerID())
                return true;

        return false;

    }

    public List<Tile> adjecentTiles()
    {
        List<Tile> list = new List<Tile>();
        foreach (Face f in halfEdge.faces())
            list.Add(f.tile);

        return list;
    }

    public List<Crossing> adjecentCrossings()
    {
        List<Crossing> list = new List<Crossing>();
        foreach (Vertex v in halfEdge.adjecentVerteces())
            list.Add(v.crossing);
        return list;
    }

    void OnMouseOver()
    {
        if (GameState.Instance.roadHover && playerID == 0)
        {
            List<Road> roads = GameState.Instance.buildableRoads;
            if (roads.FindIndex(r => r == this) >= 0)
            {
                this.GetComponent<Renderer>().material.color = GameState.Instance.humanPlayer.playerColor;
            }
        }
    }

    void OnMouseExit()
    {
        if (playerID == 0)
            this.GetComponent<Renderer>().material.color = Color.white;
    }

    void OnMouseDown()
    {
        if (GameState.Instance.roadHover)
        {
                GameState.Instance.GetPlayer(GameState.Instance.GetCurrentPlayerID()).BuildRoad(this, GameState.Instance.freeRoads > 0);
        }
    }

    /// <summary>
    /// Generates a list of roads adjecent to this road.
    /// </summary>
    /// <returns></returns>
    public List<Road> adjecentRoads()
    {
        List<Road> list = new List<Road>();
        foreach (HalfEdge he in halfEdge.adjecentHalfEdges())
            if(!list.Contains(he.road))
                list.Add(he.road);
        return list;
    }
    
}
