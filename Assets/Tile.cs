﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

 public abstract class Tile : MonoBehaviour
{
    private Face face;
    public  Face Face
    {
        get
        {
            if (face == null)
            {
                face = new Face();
                face.tile = this;
            }
            return face;
        }
        set
        {
            face = value;
        }
    }

    void Start()
    {

    }

    //Holds helper functions for reaching neighbouring objects
    #region Helper
    /// <summary>
    /// Holds a list of the roads surrounding this tile.
    /// </summary>
    private List<Road> roads;
    public List<Road> Roads
    {
        get
        {
            if (roads == null)
            {
                List<Road> list = new List<Road>();
                foreach (HalfEdge he in Face.edges())
                    list.Add(he.road);
                roads = list;
            }
            return roads;
        }
    }

     /// <summary>
     /// Holds a list of the crossings surrounding this tile
     /// </summary>
    private List<Crossing> crossings;
    public List<Crossing> Crossings
    {
        get
        {
            if (crossings == null)
            {
                List<Crossing> list = new List<Crossing>();
                foreach (Vertex v in Face.vertices())
                    list.Add(v.crossing);
                crossings = list;
            }
            return crossings;
        }
    }

    private List<Tile> tiles;
    public List<Tile> Tiles
    {
        get
        {
            if (tiles == null)
            {
                List<Tile> list = new List<Tile>();
                foreach (Face f in Face.faces())
                    list.Add(f.tile);
                tiles = list;
            }
            return tiles;
        }
    }
    #endregion Helper


    /// <summary>
    /// Just some stuff to test DCEL functionality.
    /// </summary>
    #region DCEL Pointer tests
     /*
    void OnMouseOver()
    {
        foreach (Road r in Roads)
            r.GetComponent<Renderer>().material.color = Color.red;

        foreach (Crossing c in Crossings)
            c.GetComponent<Renderer>().material.color = Color.yellow;
    }

    void OnMouseExit()
    {
        foreach (Road r in Roads)
            r.GetComponent<Renderer>().material.color = Color.white;

        foreach (Crossing c in Crossings)
            c.GetComponent<Renderer>().material.color = Color.white;
    }
     */
    #endregion DCEL Pointer tests

    /// <summary>
    /// Gets a list of edges surrounding the tile. These can be used for the DCEL.
    /// </summary>
    /// <returns>The edges.</returns>
    public List<Edge> getEdges()
    {
        Position p = new Position();
        p.x = Mathf.Round(transform.position.x * 10f) / 10f;
        p.y = Mathf.Round(transform.position.y * 10f) / 10f;
        List<Edge> list = new List<Edge>();
        list.Add(new Edge(new Position((float)p.x + 1.5f, (float)p.y + 2.6f), new Position((float)(p.x - 1.5f), (float)p.y + 2.6f)));
        list.Add(new Edge(new Position((float)p.x + 3f, (float)p.y), new Position((float)p.x + 1.5f, (float)p.y + 2.6f)));
        list.Add(new Edge(new Position((float)p.x + 1.5f, (float)p.y - 2.6f), new Position((float)p.x + 3f, (float)p.y)));
        list.Add(new Edge(new Position((float)p.x - 1.5f, (float)p.y - 2.6f), new Position((float)p.x + 1.5f, (float)p.y - 2.6f)));
        list.Add(new Edge(new Position((float)p.x - 3f, (float)p.y), new Position((float)p.x - 1.5f, (float)p.y - 2.6f)));
        list.Add(new Edge(new Position((float)p.x - 1.5f, (float)p.y + 2.6f), new Position((float)p.x - 3f, (float)p.y)));
        return list;
    }
}


