﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class TurnKeeper
{
    private int startPlayer;
    private int currentPlayer;
    private int round;
    private bool firstRoundFlip = false;
    private GameState gamestate = GameState.Instance;
    int playerCount;

    public TurnKeeper(int player, int rnd)
    {
        startPlayer = player;
        currentPlayer = player;
        round = rnd;
        playerCount = gamestate.GetPlayers().Count;
    }

    public int getCurrentPlayer()
    {
        return currentPlayer;
    }

    public void nextPlayer()
    {
        if (round == 1 &&firstRoundFlip&& currentPlayer == 1)
            gamestate.GiveInitialResources();

        if(round == 1)
        {
            
            if(firstRoundFlip == false)
            {
                if(currentPlayer == playerCount)
                {
                    firstRoundFlip = true;
                    //currentPlayer--;
                }
                else
                {
                    currentPlayer++;
                }
                return;
            }
            else
            {
                if(currentPlayer == 1)
                {
                    firstRoundFlip = false;
                    round++;
                }
                else
                {
                    currentPlayer--;
                }
                return;
            }
        }

        if(currentPlayer == playerCount)
        {
            currentPlayer = 1;
            round++;
        }
        else
        {
            currentPlayer++;
        }
    }

    public int getCurrentRound()
    {
        return round;
    }

}