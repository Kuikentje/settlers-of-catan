﻿using UnityEngine;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;

class Util
{
    private static string componentToHex(int c)
    {
        string hex = c.ToString("X");
        return hex.Length == 1 ? "0" + hex : hex;
    }

    public static string rgbToHex(Color c)
    {
        return "#" + componentToHex((int)(c.r * 255)) + componentToHex((int)(c.g * 255)) + componentToHex((int)(c.b * 255));
    }

    public static string getResourceDescription(ResourceType resource)
    {
        var type = typeof(ResourceType);
        var memInfo = type.GetMember(resource.ToString());
        var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
        var description = ((DescriptionAttribute)attributes[0]).Description;
        return description;
    }

    public static T getWeightedRandomValue<T>(Dictionary<T,int> d)
    {
        int totalWeight = 0;

        foreach (int weight in d.Values)
            totalWeight += weight;

        int r = Random.Range(0, totalWeight);

        int current = 0;
        foreach (KeyValuePair<T, int> kvp in d)
        {
            current += kvp.Value;
            if (r <= current)
                return kvp.Key;
        }
        return default(T);
    }

}