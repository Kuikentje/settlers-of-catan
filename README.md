###Git###
Hoe push je je aanpassingen:

1. git add -u #add alle changed files
2. git status #laat zien wat ge-add is en wat niet
3. git add FILENAME #optioneel als je nieuwe files wil toevoegen
4. git commit -m "COMMIT MESSAGE"
5. git pull origin master #haalt laatste versie van internet
6. mergen in visual studio als auto-merge niet welk #optioneel
7. git push origin master




naamgeving:
Wood
Ore
Stone
Wheat
Wool

### Wat Eva nu heeft gedaan: ###
1. Tiles handmatig erin gezet.
2. Tiles krijgen random resource en nummer toegewezen.
3. Er is een DCEL datastructuur met klassen voor Face, HalfEdge en Vertex (http://en.wikipedia.org/wiki/Doubly_connected_edge_list)
4. Er zijn MonoBehaviours Tile, Road en Crossing, die verwijzingen hebben naar Face, HalfEdge en Vertex
5. Er is een Board klasse die allemaal shit regelt
6. Tile klasse kan een lijst edges genereren
7. Board klasse maakt een dictionary met alle edges en bijbehorende Face
6. DCEL constructor kan van een lijst Edges een DCEL genereren.
7. DCEL instantie heeft vervolgens lijsten/dicts van halfedges en vertices die public zijn
8. Board klasse koppelt vervolgens alle DCEL objecten aan MonoBehaviour objecten

Komt er op neer dat je met de DCEL datastructuur makkelijk aangrenzende objecten (Tiles, Crossings, Roads) kan bereiken. Een paar functies heb ik al geimplementeerd om de functionaliteit te testen.

### Wat nog moet worden gedaan ###
* ~~Sommige roads en crossings worden dubbel getekend omdat ze dubbel staan in de lijsten/dicts van DCEL maar ik weet niet hoe dat komt~~
* ~~Verwijzing naar roads in halfedges lijkt niet helemaal goed te gaan~~